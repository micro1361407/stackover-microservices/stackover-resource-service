// Создание функции для вставки Header
function insertHeader() {
    // Получение элемента header
    var headerElement = document.getElementById("header");
    // Вставка HTML-кода для Header
    headerElement.innerHTML = `
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Мой Лого</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Главная <span class="sr-only">(текущая)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Ссылка</a>
                    </li>
                </ul>
            </div>
        </nav>
    `;
}

// Создание функции для вставки Sidebar
function insertSidebar() {
    // Получение элемента sidebar
    var sidebarElement = document.getElementById("sidebar");
    // Вставка HTML-кода для Sidebar
    sidebarElement.innerHTML = `
        <div class="bg-light border-right" id="sidebar-wrapper">
            <div class="sidebar-heading">Моя Боковая Панель</div>
            <div class="list-group list-group-flush">
                <a href="#" class="list-group-item list-group-item-action bg-light">Ссылка 1</a>
                <a href="#" class="list-group-item list-group-item-action bg-light">Ссылка 2</a>
                <a href="#" class="list-group-item list-group-item-action bg-light">Ссылка 3</a>
            </div>
        </div>
    `;
}

// Создание функции для вставки Footer
function insertFooter() {
    // Получение элемента footer
    var footerElement = document.getElementById("footer");
    // Вставка HTML-кода для Footer
    footerElement.innerHTML = `
        <footer class="bg-light text-center text-lg-start">
            <div class="text-center p-3">
                Мой Футер
            </div>
        </footer>
    `;
}

// Вызов функций для вставки Header, Sidebar и Footer
insertHeader();
insertSidebar();
insertFooter();