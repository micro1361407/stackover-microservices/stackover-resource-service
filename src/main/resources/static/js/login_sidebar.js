// JavaScript для создания sidebar

// Создаем элемент sidebar
var sidebar = document.createElement("div");

// Добавляем текст в sidebar
sidebar.innerHTML = "Это sidebar";

// Добавляем стили
sidebar.style.backgroundColor = "lightgreen";
sidebar.style.width = "200px";
sidebar.style.padding = "20px";
sidebar.style.float = "left";

// Добавляем sidebar в начало body
document.body.insertBefore(sidebar, document.body.firstChild);