const headerContent = `
<header class="top-bar js-top-bar top-bar__network">
  <div class="-container">
    <div class="-main">
      <a href="#" class="-logo js-gps-track">
        <span class="s-avatar">
          <div class="gravatar-wrapper-32">
            <img alt="" src="https://www.gravatar.com/avatar/a65d9343e9f934575d14d850eaa169f1?s=32&amp;d=identicon&amp;r=PG">
          </div>
        </span>
      </a>
    </div>
  </div>
</header>
`;

document.getElementById('header').innerHTML = headerContent;
