// JavaScript для создания footer

// Создаем элемент footer
var footer = document.createElement("footer");

// Добавляем текст в footer
footer.innerHTML = "Это footer";

// Добавляем стили
footer.style.backgroundColor = "lightgray";
footer.style.padding = "20px";
footer.style.textAlign = "center";

// Добавляем footer в конец body
document.body.appendChild(footer);