// JavaScript для создания header

// Создаем элемент header
var header = document.createElement("header");

// Добавляем текст в header
header.innerHTML = "Это header";

// Добавляем стили
header.style.backgroundColor = "lightblue";
header.style.padding = "20px";
header.style.textAlign = "center";

// Добавляем header в начало body
document.body.insertBefore(header, document.body.firstChild);