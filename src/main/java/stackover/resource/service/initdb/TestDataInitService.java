package stackover.resource.service.initdb;

import org.springframework.stereotype.Component;
import stackover.resource.service.entity.question.Tag;
import stackover.resource.service.entity.question.VoteQuestion;
import stackover.resource.service.entity.question.VoteTypeQuestion;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.question.answer.VoteAnswer;
import stackover.resource.service.entity.question.answer.VoteTypeAnswer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.entity.question.Question;
import stackover.resource.service.repository.entity.AnswerRepository;
import stackover.resource.service.repository.entity.QuestionRepository;
import stackover.resource.service.repository.entity.TagRepository;
import stackover.resource.service.repository.entity.UserRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class TestDataInitService {

    private final UserRepository userRepository;
    private final TagRepository tagRepository;
    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;

    public TestDataInitService(UserRepository userRepository, TagRepository tagRepository, QuestionRepository questionRepository, AnswerRepository answerRepository) {
        this.userRepository = userRepository;
        this.tagRepository = tagRepository;
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
    }

    public void createEntity() {
        createUsers();
        createTags();
        createQuestions();
        createAnswers();
        createVotes();
    }

    private void createUsers() {
        List<User> users = new ArrayList<>();

        User user1 = new User();
        user1.setId(1L);
        user1.setAccountId(1001L);
        user1.setFullName("Иванов Иван Иванович");
        user1.setPersistDateTime(LocalDateTime.now());
        user1.setCity("Москва");
        user1.setLinkSite("https://example.com");
        user1.setLinkGitHub("https://github.com/ivanov");
        user1.setLinkVk("https://vk.com/ivanov");
        user1.setAbout("О себе...");
        user1.setImageLink("https://example.com/avatar.jpg");
        user1.setLastUpdateDateTime(LocalDateTime.now());
        user1.setNickname("ivanov");

        User user2 = new User();
        user2.setId(2L);
        user2.setAccountId(1002L);
        user2.setFullName("Петров Петр Петрович");
        user2.setPersistDateTime(LocalDateTime.now());
        user2.setCity("Санкт-Петербург");
        user2.setLinkSite("https://example.com");
        user2.setLinkGitHub("https://github.com/petrov");
        user2.setLinkVk("https://vk.com/petrov");
        user2.setAbout("О себе...");
        user2.setImageLink("https://example.com/avatar.jpg");
        user2.setLastUpdateDateTime(LocalDateTime.now());
        user2.setNickname("petrov");

        users.add(user1);
        users.add(user2);

        userRepository.saveAll(users);
    }

    private void createTags() {
        List<Tag> tags = new ArrayList<>();

        Tag tag1 = new Tag();
        tag1.setId(1L);
        tag1.setName("Java");
        tag1.setDescription("Tag относится к языку Java");

        Tag tag2 = new Tag();
        tag2.setId(2L);
        tag2.setName("Spring");
        tag2.setDescription("Tag относится к фреймворку Spring");

        Tag tag3 = new Tag();
        tag3.setId(3L);
        tag3.setName("API");
        tag3.setDescription("Tag related to API design");

        Tag tag4 = new Tag();
        tag4.setId(4L);
        tag4.setName("REST");
        tag4.setDescription("Tag related to REST architecture");

        tags.add(tag1);
        tags.add(tag2);
        tags.add(tag3);
        tags.add(tag4);

        tagRepository.saveAll(tags);
    }

    private void createQuestions() {
        List<Question> questions = new ArrayList<>();

        Question question1 = new Question();
        question1.setId(1L);
        question1.setTitle("Как использовать Spring?");
        question1.setDescription("Нужна инструкция по использованию Spring.");
        question1.setPersistDateTime(LocalDateTime.now());
        question1.setUser(userRepository.findById(1L).orElseThrow(() -> new RuntimeException("User with ID 1 not found")));
        question1.getTags().add(tagRepository.findById(1L).orElseThrow(() -> new RuntimeException("Tag with ID 1 not found")));
        question1.getTags().add(tagRepository.findById(2L).orElseThrow(() -> new RuntimeException("Tag with ID 2 not found")));
        question1.setLastUpdateDateTime(LocalDateTime.now());
        question1.setIsDeleted(false);

        Question question2 = new Question();
        question2.setId(2L);
        question2.setTitle("Best practices for REST API design?");
        question2.setDescription("What are some best practices to follow when designing a RESTful API?");
        question2.setPersistDateTime(LocalDateTime.now());
        question2.setUser(userRepository.findById(2L).orElseThrow(() -> new RuntimeException("User with ID 2 not found")));
        question2.getTags().add(tagRepository.findById(3L).orElseThrow(() -> new RuntimeException("Tag with ID 3 not found")));
        question2.getTags().add(tagRepository.findById(4L).orElseThrow(() -> new RuntimeException("Tag with ID 4 not found")));
        question2.setLastUpdateDateTime(LocalDateTime.now());
        question2.setIsDeleted(false);

        questions.add(question1);
        questions.add(question2);

        questionRepository.saveAll(questions);
    }

    private void createAnswers() {
        List<Answer> answers = new ArrayList<>();

        Answer answer1 = new Answer();
        answer1.setId(1L);
        answer1.setPersistDateTime(LocalDateTime.now());
        answer1.setUpdateDateTime(LocalDateTime.now());
        answer1.setQuestion(questionRepository.findById(1L).orElseThrow(()->new RuntimeException("Question with ID 1 not found")));
        answer1.setUser(userRepository.findById(1L).orElseThrow(()->new RuntimeException("User with ID 1 not found")));
        answer1.setHtmlBody("This is an example answer.");
        answer1.setIsHelpful(true);
        answer1.setIsDeleted(false);
        answer1.setIsDeletedByModerator(false);
        answer1.setDateAcceptTime(null);

        Answer answer2 = new Answer();
        answer2.setId(2L);
        answer2.setPersistDateTime(LocalDateTime.now());
        answer2.setUpdateDateTime(LocalDateTime.now());
        answer2.setQuestion(questionRepository.findById(2L).orElseThrow(()->new RuntimeException("Question with ID 2 not found")));
        answer2.setUser(userRepository.findById(2L).orElseThrow(()->new RuntimeException("User with ID 2 not found")));
        answer2.setHtmlBody("Another example answer.");
        answer2.setIsHelpful(false);
        answer2.setIsDeleted(false);
        answer2.setIsDeletedByModerator(false);
        answer2.setDateAcceptTime(null);

        answers.add(answer1);
        answers.add(answer2);

        answerRepository.saveAll(answers);
    }

    private void createVotes() {
        List<VoteQuestion> votesQuestion = new ArrayList<>();
        List<VoteAnswer> votesAnswer = new ArrayList<>();


        User user1 = userRepository.findById(1L).orElseThrow(() ->
                new RuntimeException("User not found"));
        Question question1 = questionRepository.findById(1L).orElseThrow(() ->
                new RuntimeException("Question not found"));
        Answer answer1 = answerRepository.findById(1L).orElseThrow(() ->
                new RuntimeException("Answer not found"));

        VoteQuestion vote1 = new VoteQuestion(user1, question1, VoteTypeQuestion.UP);
        votesQuestion.add(vote1);

        VoteAnswer vote2 = new VoteAnswer(user1, answer1, VoteTypeAnswer.DOWN);
        votesAnswer.add(vote2);
    }


}
