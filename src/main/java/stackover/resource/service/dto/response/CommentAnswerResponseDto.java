package stackover.resource.service.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;

import lombok.Builder;
import org.antlr.v4.runtime.misc.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

import java.time.LocalDateTime;

@Builder
@Schema(description = "DTO-модель сущности комментария к ответу на вопрос ")
public record CommentAnswerResponseDto(
        @Schema(description = "ID комментария")
        @NotNull
        Long id,

        @Schema(description = "ID ответа")
        @NotNull
        Long answerId,
        @Schema(description = "Дата редактирования")
        LocalDateTime lastRedactionDate,

        @Schema(description = "Дата создания ответа")
        LocalDateTime persistDate,

        @Schema(description = "Текст комментария")
        @NotNull
        @NotEmpty
        String text,

        @Schema(description = "ID пользователя")
        @NotNull
        Long userId,

        @Schema(description = "Ссылка на картинку пользователя")
        String imageLink,

        @Schema(description = "Репутация пользователя")
        Long reputation
) {
    public CommentAnswerResponseDto(Long id,
                                    Long answerId,
                                    LocalDateTime lastRedactionDate,
                                    LocalDateTime persistDate,
                                    String text,
                                    Long userId,
                                    String imageLink,
                                    Long reputation) {
        this.id = id;
        this.answerId = answerId;
        this.lastRedactionDate = lastRedactionDate;
        this.persistDate = persistDate;
        this.text = text;
        this.userId = userId;
        this.imageLink = imageLink;
        this.reputation = reputation;
    }
}