package stackover.resource.service.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;



@Builder
@Schema(description = "DTO-модель сущности пользователя̆")
public record UserResponseDto(
        @Schema(description = "ID пользователя")
        @NotNull
        Long id,

        @Schema(description = "E-mail пользователя", example = "pVb2S@example.com")
        @Email
        String email,

        @Schema(description = "Полное имя пользователя", example = "John Doe")
        @NotBlank
        String fullName,

        @Schema(description = "Ссылка на аватар пользователя")
        String imageLink,

        @Schema(description = "Город пользователя", example = "Moscow")
        String city,

        @Schema(description = "Репутация пользователя")
        Long reputation,

        @Schema(description = "Дата регистрации пользователя")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
        LocalDateTime registrationDate,

        @Schema(description = "Количество голосов пользователя")
        Long votes,

        @Schema(description = "Список топ-3 тегов пользователя")
        @Nullable
        List<TagResponseDto> listTop3TagDto) implements Serializable {

        public UserResponseDto(Long id,
                               String fullName,
                               String imageLink,
                               String city,
                               Long reputation,
                               LocalDateTime registrationDate,
                               Long votes) {
            this(id, null, fullName, imageLink, city, reputation, registrationDate, votes, null);
        }
}
