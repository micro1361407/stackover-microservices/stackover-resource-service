package stackover.resource.service.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

@Builder
@Schema(description = "DTO для предоставления связанного тега")
public record RelatedTagResponseDto(
        @Schema(description = "Id тега")
        Long id,
        @Schema(description = "Название тега")
        String title,
        @Schema(description = "Количество вопросов связанных с тегом")
        Long countQuestion) {
}
