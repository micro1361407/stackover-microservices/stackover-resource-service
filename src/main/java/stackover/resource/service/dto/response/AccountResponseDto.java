package stackover.resource.service.dto.response;

import lombok.Builder;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

//TODO Тестовый DTO для получения идентификационных данных пользователя из auth-service

@Getter
@Setter
@Builder
public final class AccountResponseDto {
    private final long id;
    private final @NotBlank String email;

    public AccountResponseDto(
            long id,
            @NotBlank
            String email) {
        this.id = id;
        this.email = email;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (AccountResponseDto) obj;
        return this.id == that.id &&
               Objects.equals(this.email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email);
    }

    @Override
    public String toString() {
        return "AccountResponseDto[" +
               "id=" + id + ", " +
               "email=" + email + ']';
    }

}
