package stackover.resource.service.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDateTime;

@Schema(description = "DTO для ответа с комментарием к вопросу")
public record CommentQuestionResponseDto(
        @Schema(description = "ID комментария")
        Long id,

        @Schema(description = "ID вопроса")
        Long questionId,

        @Schema(description = "Дата последнего редактирования")
        LocalDateTime lastRedactionDate,

        @Schema(description = "Дата создания комментария")
        LocalDateTime persistDate,

        @NotNull
        @NotEmpty
        @Schema(description = "Текст комментария", required = true)
        String text,

        @NotNull
        @Schema(description = "ID автора комментария", required = true)
        Long userId,

        @Schema(description = "Ссылка на изображение")
        String imageLink,

        @Schema(description = "Репутация автора")
        Long reputation
) {}