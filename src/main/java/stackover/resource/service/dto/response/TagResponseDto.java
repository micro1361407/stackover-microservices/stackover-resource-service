package stackover.resource.service.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;

@Schema(description = "DTO тэга")
@Builder
public record TagResponseDto(
        @Schema(description = "ID тэга")
        Long id,
        @Schema(description = "Название тэга")
        String name,
        @Schema(description = "Описание тэга")
        String description
) {}

