package stackover.resource.service.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import stackover.resource.service.dto.response.TagResponseDto;

import java.util.List;

@Builder
@Schema
public record QuestionCreateRequestDto(

        @Schema(description = "Заголовок создаваемого вопроса")
        @NotNull
        String title,
        @Schema(description = "Описание создаваемого вопроса")
        @NotNull
        String description,
        @Schema(description = "Теги создаваемого вопроса")
        @NotNull
        List<TagResponseDto> tags) {

}
