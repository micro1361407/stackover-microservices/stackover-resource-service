package stackover.resource.service.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

//TODO - Заглушка, после реализации трансфера данных из Profile-Service изменить/удалить

@Builder
public record ProfileRequestDto(@NotNull Long accountId,
                                @NotBlank String email,
                                @NotBlank String fullName,
                                String city,
                                String linkSite,
                                String linkGitHub,
                                String linkVk,
                                String about,
                                String imageLink,
                                @NotBlank String nickname) {
}