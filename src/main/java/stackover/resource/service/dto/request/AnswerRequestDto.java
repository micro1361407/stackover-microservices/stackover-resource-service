package stackover.resource.service.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import stackover.resource.service.entity.question.answer.VoteTypeAnswer;

import java.time.LocalDateTime;

@Builder
@Schema(description = "DTO ответа на изменение тела комментария")
public record AnswerRequestDto(
        @Schema(description = "id ответа на вопрос")
        Long id,
        @Schema(description = "id пользователя")
        Long userId,
        @Schema(description = "id вопроса")
        Long questionId,
        @NotEmpty
        @NotBlank
        @NotNull
        @Schema(description = "текст ответа")
        String body,
        @Schema(description = "дата создания ответа")
        LocalDateTime persistDate,
        @Schema(description = "польза ответа")
        Boolean isHelpful,
        @Schema(description = "дата решения вопроса")
        LocalDateTime dateAccept,
        @Schema(description = "рейтинг ответа")
        Long countValuable,
        @Schema(description = "рейтинг юзера")
        Long countUserReputation,
        @Schema(description = "ссылка на картинку пользователя")
        String image,
        @Schema(description = "никнейм пользователя")
        String nickname,
        @Schema(description = "Количество голосов")
        Long countVote,
        @Schema(description = "тип голоса")
        VoteTypeAnswer voteTypeAnswer
) {

}