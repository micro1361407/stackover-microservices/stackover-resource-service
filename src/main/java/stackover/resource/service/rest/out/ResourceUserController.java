package stackover.resource.service.rest.out;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import stackover.resource.service.dto.response.UserResponseDto;
import stackover.resource.service.service.dto.UserDtoService;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user")
@Tag(name = "User API", description = "API для работы с пользовательскими данными")
public class ResourceUserController {

    private final UserDtoService userDtoService;

    @GetMapping("/{userId}")
    @Tag(name = "User API", description = "Api для работы с пользовательскими данными.")
    @Operation(summary = "Получение UserResponseDTO по userId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пользователь успешно найден",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserResponseDto.class))}),
            @ApiResponse(responseCode = "404", description = "Пользователь не найден",
                    content = @Content)
    })
    public ResponseEntity<UserResponseDto> getUserDtoByUserId(@PathVariable("userId") Long userId) {

        log.info("Получение UserResponseDTO по id. Method: getUserDtoById(); Input data: userId = {}", userId);

        if (userDtoService.getUserDtoByUserId(userId).isPresent()) {
            UserResponseDto userDto = userDtoService.getUserDtoByUserId(userId).get();
            log.info("Пользователь успешно найден: {}", userDto);
            return ResponseEntity.ok(userDtoService.getUserDtoByUserId(userId).get());
        } else {
            log.warn("Пользователь с userId {} не найден", userId);
            return ResponseEntity.notFound().build();
        }
    }
}
