package stackover.resource.service.rest.out;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Positive;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.entity.question.IgnoredTag;
import stackover.resource.service.service.dto.IgnoredTagDtoService;
import stackover.resource.service.service.dto.TagDtoService;
import stackover.resource.service.service.entity.IgnoredTagService;

import java.util.List;
import java.util.Optional;

@Tag(name = "ResourceTagController", description = "Контроллер который возвращает лист содержащий топ-3 тегов")
@Slf4j
@RestController
@RequestMapping("/api/user/tag/")
@Validated

public class ResourceTagController {
    private final TagDtoService tagDTOService;
    private final IgnoredTagService ignoredTagService;
    private final IgnoredTagDtoService ignoredTagDtoService;


    public ResourceTagController(TagDtoService tagDTOService,
                                 IgnoredTagService ignoredTagService, IgnoredTagDtoService ignoredTagDTOService, IgnoredTagDtoService ignoredTagDtoService) {
        this.tagDTOService = tagDTOService;
        this.ignoredTagService = ignoredTagService;
        this.ignoredTagDtoService = ignoredTagDtoService;
    }

    @Operation(summary = "Получение топ-3 тегов пользователя")
    @GetMapping("top-3tags")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список топ-3 тегов успешно получен"),
            @ApiResponse(responseCode = "400", description = "Неверный запрос или данные")
    })
    public List<TagResponseDto> getTop3TagsUser(@RequestParam @Parameter(description = "ID аккаунта пользователя", required = true)
                                                @Positive Long accountId) {
        return tagDTOService.getTop3TagsByUserId(accountId);
    }

    @Operation(summary = " Добавление тега в игнорируемые теги")
    @PostMapping("{tagId}/ignored")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Тег успешно добавлен в игнорируемые теги"),
            @ApiResponse(responseCode = "400", description = "Неверный запрос или данные")
    })
    public ResponseEntity<TagResponseDto> addTagToIgnoredTag(
            @PathVariable Long tagId,
            @RequestParam @Parameter(description = "id аккаунта") @Positive Long accountId) {

        log.info("addTagToIgnoreTag: {}", tagId);
        Optional<IgnoredTag> ignoredTag = ignoredTagService.addToIgnoredTag(tagId, accountId);
        if (ignoredTag.isPresent()) {
            Optional<TagResponseDto> tagResponseDTO = ignoredTagDtoService.toTagResponseDto(ignoredTag.get());
            log.info("return tagResponseDto IgnoredTag from accountId {}:", accountId);
            return new ResponseEntity<>(tagResponseDTO.get(), HttpStatus.OK);
        } else {
            log.error("Invalid request or data");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
