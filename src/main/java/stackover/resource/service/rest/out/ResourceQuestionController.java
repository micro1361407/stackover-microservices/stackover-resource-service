package stackover.resource.service.rest.out;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import stackover.resource.service.dto.request.QuestionCreateRequestDto;
import stackover.resource.service.dto.response.CommentQuestionResponseDto;
import stackover.resource.service.dto.response.QuestionResponseDto;
import stackover.resource.service.entity.question.Question;
import stackover.resource.service.feign.AuthServiceFeignClient;
import stackover.resource.service.service.dto.QuestionResponseDtoService;
import stackover.resource.service.service.dto.impl.QuestionDtoServiceImpl;
import stackover.resource.service.service.entity.CommentQuestionService;
import stackover.resource.service.service.entity.QuestionService;

import java.util.List;
import java.util.Optional;

@Tag(name = "ResourceQuestionController", description = "Question контроллер")
@Slf4j
@Validated
@RequestMapping("/api/user/question")
@RestController
public class ResourceQuestionController {

    private final QuestionService questionService;
    private final QuestionResponseDtoService questionResponseDtoService;
    private final QuestionDtoServiceImpl questionDtoService;
    private final CommentQuestionService commentQuestionService;
    private final AuthServiceFeignClient authServiceFeignClient;

    public ResourceQuestionController(QuestionService questionService,
                                      QuestionResponseDtoService questionResponseDtoService,
                                      QuestionDtoServiceImpl questionDtoService,
                                      CommentQuestionService commentQuestionService,
                                      @Qualifier("stackover.resource.service.feign.AuthServiceFeignClient") AuthServiceFeignClient authServiceFeignClient) {
        this.questionService = questionService;
        this.questionResponseDtoService = questionResponseDtoService;
        this.questionDtoService = questionDtoService;
        this.commentQuestionService = commentQuestionService;
        this.authServiceFeignClient = authServiceFeignClient;
    }

    @Operation(description = "api которое добавляет новый вопрос, возвращает QuestionResponseDto")
    @PostMapping
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешное создание вопроса"),
            @ApiResponse(responseCode = "400", description = "Неверный запрос или данные")
    })

    public ResponseEntity<QuestionResponseDto> addNewQuestion(
            @RequestParam @Positive Long accountId, @Valid @RequestBody QuestionCreateRequestDto questionCreateRequestDto) {

        Optional<Question> question = questionService.saveQuestion(accountId,questionCreateRequestDto);
        if (question.isPresent()) {

            Optional<QuestionResponseDto> questionResponseDto = questionResponseDtoService.toDto(question.get());
            log.info("questionResponseDto created: {}", questionResponseDto);
            return new ResponseEntity<>(questionResponseDto.get(), HttpStatus.CREATED);

        } else {

            log.error("questionCreateRequestDto ERROR: {}", questionCreateRequestDto);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Operation(
            summary = "Получить все комментарии вопроса",
            description = "Возвращает список комментариев с информацией об авторе",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное получение комментариев"),
                    @ApiResponse(responseCode = "404", description = "Вопрос не найден")
            }
    )
    @GetMapping("/{id}/comment")
    public ResponseEntity<List<CommentQuestionResponseDto>> getAllCommentsOnQuestion(
            @Parameter(description = "ID вопроса", required = true, example = "123")
            @PathVariable Long id) {

        log.info("Получение комментариев для вопроса ID: {}", id);
        long startTime = System.currentTimeMillis();

        List<CommentQuestionResponseDto> response = commentQuestionService.getAllCommentsForQuestion(id);

        log.debug("Время выполнения запроса: {} мс", System.currentTimeMillis() - startTime);
        return ResponseEntity.ok(response);
    }

    @Operation(summary = "Получение вопроса по ID",
            description = "Возвращает DTO вопроса"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Вопрос найден"),
            @ApiResponse(responseCode = "404", description = "Вопрос не найден")
    })
    @GetMapping("/{questionId}")
    public ResponseEntity<QuestionResponseDto> getQuestionById(
            @Parameter(name = "questionId", description = "ID вопроса")
            @PathVariable Long questionId,
            @Parameter(name = "accountId", description = "ID пользователя")
            @RequestParam @Positive Long accountId) {

        log.info("Запрос на получение вопроса по ID: {}, для пользователя ID: {}", questionId, accountId);

        if (!authServiceFeignClient.isAccountExist(accountId)) {
            log.info("Получить вопрос не удалось по причине: Пользователь с данным ID {} не найден", accountId);
            return ResponseEntity.notFound().build();
        }

        Optional<QuestionResponseDto> questionResponseDto = questionDtoService.getQuestionDtoByQuestionIdAndUserId(questionId, accountId);

        if (questionResponseDto.isEmpty()) {
            log.info("Получить вопрос не удалось по причине: вопрос с ID {} для пользователя ID {} не найден", questionId, accountId);
            return ResponseEntity.notFound().build();
        }

        log.info("Получен вопрос: {}", questionResponseDto.get());
        return ResponseEntity.ok(questionResponseDto.get());
    }
}
