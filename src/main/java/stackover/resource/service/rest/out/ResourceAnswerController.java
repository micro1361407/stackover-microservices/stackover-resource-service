package stackover.resource.service.rest.out;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.dto.response.AnswerResponseDto;
import stackover.resource.service.dto.response.CommentAnswerResponseDto;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.service.*;
import stackover.resource.service.service.dto.AnswerDtoService;
import stackover.resource.service.service.dto.AnswerResponseDtoService;
import stackover.resource.service.service.entity.AnswerService;
import stackover.resource.service.service.entity.CommentAnswerService;
import stackover.resource.service.entity.question.answer.CommentAnswer;
import stackover.resource.service.service.entity.QuestionService;
import stackover.resource.service.service.entity.impl.QuestionServiceImpl;

import java.util.List;
import java.util.Optional;

@Tag(name = "Answer API", description = "API проголосовать за Answer, уменьшает оценку ответа")
@RestController
@RequestMapping("/api/user/question/{questionId}/answer")
@Slf4j
@RequiredArgsConstructor
public class ResourceAnswerController {


    private final VotingService votingService;
    private final VoteAnswerService voteAnswerService;
    private final AnswerResponseDtoService answerResponseDtoService;
    private final AnswerService answerService;
    private final CommentAnswerService commentService;
    private final AnswerDtoService answerDtoService;
    private final QuestionService questionService;


    @Operation(summary = "Получение списка всех ответов",
            description = "возвращает DTO все ответов по accountId и questionId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список ответов успешно получен"),
            @ApiResponse(responseCode = "400", description = "Неверный запрос или данные")
    })
    @GetMapping()
    public ResponseEntity<List<AnswerResponseDto>> getAllAnswers(@PathVariable @Positive Long questionId,
                                                 @RequestParam @Positive Long accountId) {
        log.info("Получение списка ответов на questionId: {} от userId: {}", questionId, accountId);
//       TODO: реализовать auth

        if (questionService.findByIdAndUser_AccountId(questionId, accountId).isEmpty()) {
            log.info("Ответы не получены. Причина: questionId {} from account_id {} not found", questionId, accountId);
            return ResponseEntity.notFound().build();
        }

        log.info("Ответы получены. Вопрос: {} Пользователь: {}", questionId, accountId);

        return ResponseEntity.ok(answerDtoService.getAnswersDtoByQuestionId(questionId, accountId));
    }

    @PostMapping("/{answerId}/upVote")
    @Operation(summary = "Upvote an answer", description = "Returns the total number of votes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Репутация автора ответа повышена",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Integer.class))}),
            @ApiResponse(responseCode = "404", description = "Ответ не найден",
                    content = @Content)
    })
    public ResponseEntity<Long> upVoteAnswer(
            @PathVariable Long questionId,
            @PathVariable Long answerId,
            @RequestParam @Positive Long accountId) {
        try {
            votingService.upVoteAnswer(questionId, answerId, accountId);
            return ResponseEntity.ok(voteAnswerService.countByAnswerId(answerId));
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage());
            return ResponseEntity.badRequest().body(null);
        } catch (RuntimeException e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping("/{answerId}/downVote")
    @Operation(summary = "Downvote an answer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ответ успешно понижен в рейтинге",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Integer.class))}),
            @ApiResponse(responseCode = "404", description = "Ответ не найден",
                    content = @Content)
    })
    public ResponseEntity<Long> downVoteAnswer(
            @PathVariable Long questionId,
            @PathVariable Long answerId,
            @RequestParam @Positive Long accountId) {
        log.info("Создается голосование для вопроса {}, ответа {}, пользователем {}.", questionId, answerId, accountId);
        return ResponseEntity.ok(votingService.downVoteAnswer(questionId, answerId, accountId));
    }


    @Operation(summary = "Обновить тело комментария", responses = {
            @ApiResponse(responseCode = "200", description = "Комментарий успешно обновлён"),
            @ApiResponse(responseCode = "400", description = "Комментарий не найден"),
    })
    @PutMapping("/{answerId}/body")
    public ResponseEntity<AnswerResponseDto> updateAnswerBody(
            @PathVariable Long questionId,
            @PathVariable Long answerId,
            @RequestBody AnswerRequestDto answerRequestDto,
            @RequestParam @Parameter(description = "id аккаунта") @Positive Long accountId) {

        Optional<Answer> updatedAnswer = answerService.updateAnswer(answerId, questionId, answerRequestDto, accountId);
        if (updatedAnswer.isPresent()) {
            Optional<AnswerResponseDto> answerResponseDto = answerResponseDtoService.toDto(updatedAnswer.get());
            log.info(String.format("ResponseEntity created with AnswerResponseDtoRepository: %s", answerResponseDto));
            return new ResponseEntity<>(answerResponseDto.get(), HttpStatus.OK);
        } else {
            log.error(String.format("Пользователя с id = %s не существует", accountId));
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PostMapping("/{answerId}/comment")
    @Operation(summary = "Добавление комментария к ответу")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Комментарий успешно добавлен",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CommentAnswerResponseDto.class))}),
            @ApiResponse(responseCode = "400", description = "Неверные параметры запроса", content = @Content),
            @ApiResponse(responseCode = "404", description = "Пользователь или ответ не найдены", content = @Content)
    })
    public ResponseEntity<Optional<CommentAnswer>> addCommentToAnswer(
            @PathVariable @Positive Long questionId,
            @PathVariable @Positive Long answerId,
            @RequestParam @Positive Long accountId,
            @RequestBody @NotBlank String commentText) {

        log.info("Добавление комментария к ответу. Method: addCommentToAnswer(); Input data: questionId = {}, answerId = {}, accountId = {}, commentText = {}",
                questionId, answerId, accountId, commentText);


        Optional<CommentAnswer> responseDto = commentService.addComment(questionId, answerId, commentText, accountId);

        log.info("Comment added to answer with ID {} for question with ID {} by user with ID {}", answerId, questionId/*, responseDto.getUserId()*/);

        return ResponseEntity.status(HttpStatus.CREATED).body(responseDto);
    }

    @DeleteMapping("/{answerId}")
    @Operation(summary = "Пометка ответа как удаленного")
    public ResponseEntity<Void> markAnswerToDelete(@Parameter(description = "Id вопроса", required = true)
                                                   @PathVariable Long questionId,
                                                   @Parameter(description = "Id ответа", required = true)
                                                   @PathVariable Long answerId) {
        try {
            answerService.markAnswerToDelete(answerId);
            log.info("Ответ с Id {} помечен как удаленный", answerId);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } catch (EntityNotFoundException e) {
            log.error("Ответ с ID {} не найден", answerId);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

}

