package stackover.resource.service.service;

public interface VotingService {
     Long downVoteAnswer(Long questionId, Long answerId, Long accountId);

     void upVoteAnswer(Long questionId, Long answerId, Long accountId);
}
