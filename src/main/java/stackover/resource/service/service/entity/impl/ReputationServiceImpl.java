package stackover.resource.service.service.entity.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.entity.user.reputation.Reputation;
import stackover.resource.service.entity.user.reputation.ReputationType;
import stackover.resource.service.repository.entity.ReputationRepository;
import stackover.resource.service.service.*;

import java.util.Optional;

@Service

public class ReputationServiceImpl extends AbstractServiceImpl<Reputation, Long> implements ReputationService {
    private final ReputationRepository reputationRepository;


    @Autowired
    public ReputationServiceImpl(ReputationRepository reputationRepository) {
        super(reputationRepository);
        this.reputationRepository = reputationRepository;

    }


    @Override
    public Optional<Reputation> findByAnswerAndAndAuthor(Answer answer, User user) {
        return reputationRepository.findByAnswerAndAndAuthor(answer, user);
    }

}

