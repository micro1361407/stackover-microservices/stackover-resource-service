package stackover.resource.service.service.entity.impl;

import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import stackover.resource.service.converters.QuestionConverter;
import stackover.resource.service.dto.request.QuestionCreateRequestDto;
import stackover.resource.service.entity.question.Question;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.feign.AuthServiceFeignClient;
import stackover.resource.service.repository.entity.QuestionRepository;
import stackover.resource.service.service.entity.QuestionService;
import stackover.resource.service.service.entity.UserService;


import java.util.Optional;

@Slf4j
@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final AuthServiceFeignClient authServiceFeignClient;
    private final UserService userService;
    private final QuestionConverter questionConverter;

    public QuestionServiceImpl(QuestionRepository questionRepository, @Qualifier("stackover.resource.service.feign.AuthServiceFeignClient") AuthServiceFeignClient authServiceFeignClient, UserService userService, QuestionConverter questionConverter) {
        this.questionRepository = questionRepository;
        this.authServiceFeignClient = authServiceFeignClient;
        this.userService = userService;
        this.questionConverter = questionConverter;
    }

    @Override
    public Optional<Question> findByIdAndUser_AccountId(Long id, Long accountId) {
        return questionRepository.findByIdAndUser_AccountId(id, accountId);
    }

    @Override
    public Optional<Question> saveQuestion(Long accountId, QuestionCreateRequestDto questionCreateRequestDto) {

        if (!authServiceFeignClient.isAccountExist(accountId)) {
            throw new EntityNotFoundException("Account with ID " + accountId + " not found");
        }

        User user = userService.findByAccountId(accountId)
                .orElseThrow(() -> new EntityNotFoundException("User with account ID " + accountId + " not found"));

        Question question = questionConverter.toEntity(questionCreateRequestDto, user);

        log.info("Saved question {}", question);

        return Optional.of(questionRepository.save(question));


    }

    public boolean existsById(Long questionId) {
        return questionRepository.existsById(questionId);
    }
}