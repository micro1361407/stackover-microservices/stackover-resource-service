package stackover.resource.service.service.entity;

import stackover.resource.service.dto.response.CommentQuestionResponseDto;

import java.util.List;

public interface CommentQuestionService {
    List<CommentQuestionResponseDto> getAllCommentsForQuestion(Long questionId);
}
