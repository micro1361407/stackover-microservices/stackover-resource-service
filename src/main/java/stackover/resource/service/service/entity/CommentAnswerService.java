package stackover.resource.service.service.entity;

import stackover.resource.service.entity.question.answer.CommentAnswer;

import java.util.Optional;

public interface CommentAnswerService {
    Optional <CommentAnswer> addComment(Long questionId, Long answerId, String comment, Long accountId);

}


