package stackover.resource.service.service.entity.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.repository.entity.UserRepository;
import stackover.resource.service.service.entity.TagService;
import stackover.resource.service.service.entity.UserService;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, TagService tagService) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findByAccountId(Long accountId) {
        return userRepository.findByAccountId(accountId);
    }
}