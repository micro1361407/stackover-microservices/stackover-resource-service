package stackover.resource.service.service.entity.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stackover.resource.service.dto.response.CommentQuestionResponseDto;
import stackover.resource.service.exception.QuestionException;
import stackover.resource.service.repository.dto.CommentQuestionDtoRepository;
import stackover.resource.service.service.entity.CommentQuestionService;
import stackover.resource.service.service.entity.QuestionService;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CommentQuestionServiceImpl implements CommentQuestionService {

    private final CommentQuestionDtoRepository commentQuestionDtoRepository;
    private final QuestionService questionService;

    public List<CommentQuestionResponseDto> getAllCommentsForQuestion(Long questionId) {
        log.debug("Начало обработки запроса на получение комментариев для вопроса с ID: {}", questionId);

        if (!questionService.existsById(questionId)) {
            log.warn("Вопрос с ID {} не найден", questionId);
            throw new QuestionException(questionId);
        }

        List<CommentQuestionResponseDto> result = commentQuestionDtoRepository.getAllQuestionCommentDtoById(questionId);
        log.info("Найдено {} комментариев для вопроса с ID: {}", result.size(), questionId);

        return result;
    }
}