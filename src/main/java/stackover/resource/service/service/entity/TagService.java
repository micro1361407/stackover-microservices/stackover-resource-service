package stackover.resource.service.service.entity;

import stackover.resource.service.entity.question.Tag;


public interface TagService {

    void save(Tag newTag);

    Tag findByName(String name);
}
