package stackover.resource.service.service.entity.impl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.question.answer.CommentAnswer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.feign.AuthServiceFeignClient;
import stackover.resource.service.feign.AuthServiceFeignClientStub;
import stackover.resource.service.repository.entity.CommentAnswerRepository;
import stackover.resource.service.service.entity.AnswerService;
import stackover.resource.service.service.entity.CommentAnswerService;
import stackover.resource.service.service.entity.UserService;

import java.util.Optional;

@Slf4j
@Service
public class CommentAnswerServiceImpl implements CommentAnswerService {

    private final CommentAnswerRepository commentAnswerRepository;
    private final UserService userService;
    private final AuthServiceFeignClient authServiceFeignClient;
    private final AuthServiceFeignClientStub authServiceFeignClientStub;
    private final AnswerService answerService;

    public CommentAnswerServiceImpl(CommentAnswerRepository commentAnswerRepository, UserService userService, AuthServiceFeignClient authServiceFeignClient, AuthServiceFeignClientStub authServiceFeignClientStub, AnswerService answerService) {
        this.commentAnswerRepository = commentAnswerRepository;
        this.userService = userService;
        this.authServiceFeignClient = authServiceFeignClient;
        this.authServiceFeignClientStub = authServiceFeignClientStub;
        this.answerService = answerService;
    }

    @Override
    @Transactional
    public Optional<CommentAnswer> addComment(Long questionId, Long answerId, String commentText, Long accountId) {
        if (!authServiceFeignClientStub.isAccountExist(accountId)) {
            log.error("Account with ID {} does not exist", accountId);
            return Optional.empty();
        }
      Optional <User> user = userService.findByAccountId(accountId);
        Optional<Answer> answer = answerService.findById(answerId);

        CommentAnswer commentAnswer = new CommentAnswer();
        commentAnswer.setId(questionId);
        commentAnswer.setUser(user.get());
        commentAnswer.setText(commentText);
        return Optional.of(commentAnswer);
    }
}