package stackover.resource.service.service.entity;

import stackover.resource.service.entity.user.User;

import java.util.Optional;

public interface UserService {

    Optional<User> findByAccountId(Long accountId);
}
