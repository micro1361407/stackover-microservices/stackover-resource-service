package stackover.resource.service.service.entity.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import stackover.resource.service.entity.question.IgnoredTag;
import stackover.resource.service.entity.question.Tag;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.exception.TagAlreadyExistsException;
import stackover.resource.service.exception.TagNotFoundException;
import stackover.resource.service.feign.AuthServiceFeignClient;
import stackover.resource.service.repository.entity.IgnoredTagRepository;
import stackover.resource.service.repository.entity.TagRepository;
import stackover.resource.service.service.entity.IgnoredTagService;
import stackover.resource.service.service.entity.UserService;

import java.util.Optional;


@Slf4j
@Service
public class IgnoredTagServiceImpl implements IgnoredTagService {

    private final UserService userService;
    private final TagRepository tagRepository;
    private final IgnoredTagRepository ignoredTagRepository;
    private final AuthServiceFeignClient authServiceFeignClient;

    public IgnoredTagServiceImpl( UserService userService, TagRepository tagRepository,
                                  IgnoredTagRepository ignoredTagRepository, AuthServiceFeignClient authServiceFeignClient) {
        this.userService = userService;
        this.tagRepository = tagRepository;
        this.ignoredTagRepository = ignoredTagRepository;
        this.authServiceFeignClient = authServiceFeignClient;
    }

    @Override
    public Optional<IgnoredTag> addToIgnoredTag(Long tagId, Long accountId) {
        if (!authServiceFeignClient.isAccountExist(accountId)) {
            log.error("Account with ID {} does not exist", accountId);
            return Optional.empty();
        }
        Optional<User> user = userService.findByAccountId(accountId);
        Tag tag = tagRepository.findById(tagId).orElseThrow(
                () -> new TagNotFoundException(String.format("тег с id %s не найден", tagId)));
        if (user.isPresent()) {
            IgnoredTag ignoredTag = new IgnoredTag();
            ignoredTag.setIgnoredTag(tag);
            ignoredTag.setUser(user.get());
            if (!ignoredTagRepository.exists(Example.of(ignoredTag))) {
                log.info("Adding tag {} to ignored tag", tag.getName());
                return Optional.of(ignoredTagRepository.save(ignoredTag));
            } else {
                log.error("Ignored tag {} already exists", tag.getName());
                throw new TagAlreadyExistsException("тег уже был добавлен в игнорируемые ранее");
            }
        } else {
            log.error("User with ID {} does not exist", accountId);
            return Optional.empty();
        }
    }
}
