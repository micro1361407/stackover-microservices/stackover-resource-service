package stackover.resource.service.service.entity;

import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.entity.question.answer.Answer;

import java.util.Optional;

public interface AnswerService {
    Optional<Answer> updateAnswer(Long answerId, Long questionId, AnswerRequestDto answerRequestDto, Long accountId);

    Optional<Answer> findById(Long answerId);

    void markAnswerToDelete(Long answerId);
}
