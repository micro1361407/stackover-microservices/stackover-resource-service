package stackover.resource.service.service.entity.impl;


import org.springframework.stereotype.Service;
import stackover.resource.service.entity.question.Tag;
import stackover.resource.service.repository.entity.TagRepository;
import stackover.resource.service.service.entity.TagService;


@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }


    @Override
    public void save(Tag newTag) {
        tagRepository.save(newTag);
    }

    @Override
    public Tag findByName(String name) {
        return tagRepository.findByName(name);
    }


}