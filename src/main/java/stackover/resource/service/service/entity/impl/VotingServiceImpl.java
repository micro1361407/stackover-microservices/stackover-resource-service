package stackover.resource.service.service.entity.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.question.answer.VoteAnswer;
import stackover.resource.service.entity.question.answer.VoteTypeAnswer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.entity.user.reputation.Reputation;

import stackover.resource.service.entity.user.reputation.ReputationType;


import stackover.resource.service.exception.AccountNotAvailableException;
import stackover.resource.service.exception.AnswerException;
import stackover.resource.service.feign.AuthServiceFeignClient;

import stackover.resource.service.service.ReputationService;
import stackover.resource.service.service.VoteAnswerService;
import stackover.resource.service.service.VotingService;

import stackover.resource.service.service.entity.AnswerService;
import stackover.resource.service.service.entity.UserService;

import java.util.Collections;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class VotingServiceImpl implements VotingService {
    private final AnswerService answerService;
    private final VoteAnswerService voteAnswerService;
    private final UserService userService;
    private final ReputationService reputationService;
    private final AuthServiceFeignClient authServiceFeignClient;

    @Override
    public Long downVoteAnswer(Long questionId, Long answerId, Long accountId) {

        log.info("Инициализация downvote для вопроса {}, ответа {}, от пользователя {}", questionId, answerId, accountId);

        Answer answer = answerService.findById(answerId)
                .orElseThrow(() -> new AnswerException("Ответ не найден"));


        if (!answer.getQuestion().getId().equals(questionId)) {
            throw new AnswerException(String.format("Ответ %s не относится к вопросу %s", answerId, questionId));
        }

        if (answer.getUser().getId().equals(accountId)) {
            log.warn("Пользователь {} пытается проголосовать за свой собственный ответ {}", accountId, answerId);
            throw new AnswerException("Пользователь не может голосовать за свой собственный ответ");
        }

        User userSender = userService.findByAccountId(accountId)
                .orElseThrow(() -> new AccountNotAvailableException(String.format("Пользователь не найден: %s", accountId)));
        log.debug("UserSender: {}", userSender);

        User answerAuthor = answer.getUser();
        log.debug("answerAuthor: {}", answerAuthor);


        Optional<VoteAnswer> voteAnswerOptional = voteAnswerService.findByAnswerAndUser(answer, userSender);
        VoteAnswer voteAnswer;
        Reputation reputation;


        if (voteAnswerOptional.isEmpty()) {
            voteAnswer = new VoteAnswer(userSender, answer, VoteTypeAnswer.DOWN);
            voteAnswerService.save(voteAnswer);

            reputation = new Reputation();
            reputation.setType(ReputationType.VOTE_ANSWER);
            reputation.setAuthor(answerAuthor);
            reputation.setSender(userSender);
            reputation.setAnswer(answer);
            reputation.setCount(-5);
            reputationService.save(reputation);

            log.debug("Добавлен новый downvote: {}", voteAnswer);
            log.debug("Репутация уменьшена на 5: {}", reputation);

        } else {
            voteAnswer = voteAnswerOptional.get();
            if (voteAnswer.getVoteTypeAnswer().equals(VoteTypeAnswer.UP)) {
                voteAnswer.setVoteTypeAnswer(VoteTypeAnswer.DOWN);
                voteAnswerService.save(voteAnswer);

                Optional<Reputation> optionalReputation = reputationService.findByAnswerAndAndAuthor(answer, userSender);
                if (optionalReputation.isPresent()) {
                    reputation = optionalReputation.get();
                    reputation.setCount(-5);
                    reputationService.save(reputation);
                } else {
                    reputation = new Reputation();
                    reputation.setType(ReputationType.VOTE_ANSWER);
                    reputation.setAuthor(answerAuthor);
                    reputation.setSender(userSender);
                    reputation.setAnswer(answer);
                    reputation.setCount(-5);
                    reputationService.save(reputation);
                }

                log.debug("Голос изменен на downvote: {}", voteAnswer);
                log.debug("Репутация обновлена до -5: {}", reputation);
            }
        }

        long totalVotes = voteAnswerService.countByAnswerId(answerId);
        log.info("Общее количество downvote голосов за ответ {}: {}", answerId, totalVotes);

        return totalVotes;
    }

    @Override
    public void upVoteAnswer(Long questionId, Long answerId, Long accountId) {

        if (!authServiceFeignClient.isAccountExist(accountId)) {
            throw new IllegalArgumentException("Account with ID {} does not exist");
        }

        Answer answer = answerService.findById(answerId).orElseThrow(() -> new RuntimeException("Ответ с таким id не найден"));

        if (answer.getUser().getId().equals(accountId)) {
            throw new IllegalArgumentException("Пользователь не может проголосовать за свой ответ!");
        }

        Optional<VoteAnswer> vote = voteAnswerService.findByAnswerAndUser(answer, userService.findByAccountId(accountId).get());

        if (vote.isPresent() &&
                vote.get().getVoteTypeAnswer().equals(VoteTypeAnswer.UP) && vote.get().getUser().getId().equals(accountId)) {
            throw new IllegalArgumentException("Пользователь уже голосовал за этот ответ таким образом!");
        }

        Reputation reputation = new Reputation();
            reputation.setAnswer(answer);
            reputation.setAuthor(userService.findByAccountId(answer.getUser().getId()).get());
            reputation.setType(ReputationType.VOTE_ANSWER);
            reputation.setCount(+10);
            reputationService.save(reputation);
            VoteAnswer voteAnswer = new VoteAnswer();
            voteAnswer.setAnswer(answer);
            voteAnswer.setUser(userService.findByAccountId(accountId).get());
            voteAnswer.setVoteTypeAnswer(VoteTypeAnswer.UP);
            voteAnswerService.save(voteAnswer);
    }
    }

