package stackover.resource.service.service.entity;

import stackover.resource.service.dto.request.QuestionCreateRequestDto;
import stackover.resource.service.entity.question.Question;

import java.util.Optional;

public interface QuestionService {

    Optional<Question> findByIdAndUser_AccountId(Long id, Long accountId);

    Optional<Question> saveQuestion(Long accountId, QuestionCreateRequestDto questionCreateRequestDto);

    boolean existsById(Long questionId);
}
