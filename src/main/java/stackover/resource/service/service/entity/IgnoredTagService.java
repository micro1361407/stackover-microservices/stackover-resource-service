package stackover.resource.service.service.entity;

import stackover.resource.service.entity.question.IgnoredTag;

import java.util.Optional;

public interface IgnoredTagService {

    Optional<IgnoredTag> addToIgnoredTag(Long tagId, Long accountId);
}
