package stackover.resource.service.service.entity.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import stackover.resource.service.entity.question.Question;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.question.answer.VoteAnswer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.repository.entity.VoteAnswerRepository;
import stackover.resource.service.service.QuestionService;
import stackover.resource.service.service.VoteAnswerService;

import java.util.Optional;

@Slf4j
@Service
public class VoteAnswerServiceImpl extends AbstractServiceImpl<VoteAnswer, Long> implements VoteAnswerService {
    private VoteAnswerRepository voteAnswerRepository;

    @Autowired
    public VoteAnswerServiceImpl(VoteAnswerRepository voteAnswerRepository) {
        super(voteAnswerRepository);
        this.voteAnswerRepository = voteAnswerRepository;
    }

    @Override
    public Optional<VoteAnswer> findByAnswerAndUser(Answer answer, User user) {
        return voteAnswerRepository.findByAnswerAndUser(answer, user);
    }

    @Override
    public Long countByAnswerId(Long answerId) {
        return voteAnswerRepository.countByAnswerId(answerId);
    }
}
