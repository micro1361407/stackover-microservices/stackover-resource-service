package stackover.resource.service.service.entity.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.feign.AuthServiceFeignClient;
import stackover.resource.service.repository.entity.AnswerRepository;
import stackover.resource.service.service.entity.AnswerService;
import stackover.resource.service.service.entity.UserService;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
@Service
public class AnswerServiceImpl implements AnswerService {

    private final AnswerRepository answerRepository;
    private final UserService userService;
    private final AuthServiceFeignClient authServiceFeignClient;

    public AnswerServiceImpl(AnswerRepository answerRepository, UserService userService, AuthServiceFeignClient authServiceFeignClient) {
        this.answerRepository = answerRepository;
        this.userService = userService;
        this.authServiceFeignClient = authServiceFeignClient;
    }

    @Override
    @Transactional
    public Optional<Answer> updateAnswer(Long answerId, Long questionId, AnswerRequestDto answerRequestDto, Long accountId) {



        if (!authServiceFeignClient.isAccountExist(accountId)) {
            log.error("Account with ID {} does not exist", accountId);
            return Optional.empty();
        }
        Optional<User> user = userService.findByAccountId(accountId);
        if (user.isPresent()) {
            Answer answer = answerRepository.findByIdAndQuestionId(answerId, questionId)
                    .orElseThrow(() -> new RuntimeException("Answer not found"));
            answer.setHtmlBody(answerRequestDto.body());
            log.info("Answer update: {}", answer);
            answerRepository.save(answer);
            return Optional.of(answer);
        } else {
            log.error("User with ID {} does not exist", accountId);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Answer> findById(Long id) {
        return answerRepository.findById(id);
    }

    @Override
    @Transactional
    public void markAnswerToDelete(Long answerId) {
        Answer answer = answerRepository.findById(answerId)
                .orElseThrow(() -> new RuntimeException("Не найден ответ с id = " + answerId));
        answer.setIsDeleted(true);
        answerRepository.save(answer);
    }
}