package stackover.resource.service.service.dto;


import stackover.resource.service.dto.response.CommentAnswerResponseDto;
import stackover.resource.service.entity.question.answer.CommentAnswer;

import java.util.Optional;

public interface CommentDtoService {
        Optional<CommentAnswerResponseDto> toDto(CommentAnswer commentAnswer);
}

