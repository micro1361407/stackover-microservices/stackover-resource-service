package stackover.resource.service.service.dto.impl;

import org.springframework.stereotype.Service;
import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.entity.question.IgnoredTag;
import stackover.resource.service.repository.dto.IgnoredTagDtoRepository;
import stackover.resource.service.service.dto.IgnoredTagDtoService;


import java.util.Optional;

@Service
public class IgnoredTagDtoServiceImpl implements IgnoredTagDtoService {

    private final IgnoredTagDtoRepository ignoredTagDtoRepository;

    public IgnoredTagDtoServiceImpl(IgnoredTagDtoRepository ignoredTagDTORepository) {
        this.ignoredTagDtoRepository = ignoredTagDTORepository;
    }

    @Override
    public Optional<TagResponseDto> toTagResponseDto(IgnoredTag ignoredTag) {
        var tagResponseDto = ignoredTagDtoRepository.toTagResponseDto(
                ignoredTag.getIgnoredTag().getId(),
                ignoredTag.getUser().getId()
        );

        return tagResponseDto.map(tagResponseDTO -> TagResponseDto.builder()
                .id(tagResponseDTO.id())
                .name(tagResponseDTO.name())
                .description(tagResponseDTO.description())
                .build());
    }
}