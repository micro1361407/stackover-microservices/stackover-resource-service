package stackover.resource.service.service.dto;

import stackover.resource.service.dto.response.QuestionResponseDto;
import stackover.resource.service.entity.question.Question;

import java.util.Optional;

public interface QuestionResponseDtoService {

    Optional<QuestionResponseDto> toDto(Question question);

}
