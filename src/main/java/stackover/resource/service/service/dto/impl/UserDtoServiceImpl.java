package stackover.resource.service.service.dto.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import stackover.resource.service.dto.request.ProfileRequestDto;
import stackover.resource.service.dto.response.UserResponseDto;
import stackover.resource.service.feign.AuthServiceFeignClient;
import stackover.resource.service.feign.ProfileServiceFeignClient;
import stackover.resource.service.repository.dto.UserDtoRepository;
import stackover.resource.service.service.dto.TagDtoService;
import stackover.resource.service.service.dto.UserDtoService;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserDtoServiceImpl implements UserDtoService {
    private final AuthServiceFeignClient authServiceFeignClient;
    private final ProfileServiceFeignClient profileServiceFeignClient;
    private final UserDtoRepository userDtoRepository;
    private final TagDtoService tagDtoService;

    @Override
    @Transactional(readOnly = true)
    public Optional<UserResponseDto> getUserDtoByUserId(Long userId) {

        //TODO метод использует обращение к сервису stackover-auth-service с проверкой существования аккаунта. К удалению.

        if (Boolean.FALSE.equals(authServiceFeignClient.isAccountExist(userId))) {
            return Optional.empty();
        }

        ResponseEntity<ProfileRequestDto> profileByAccountId =
                profileServiceFeignClient.getProfileByAccountId(authServiceFeignClient.getAccountById(userId).getBody().getId());

        var user = userDtoRepository.getUserDtoByUserId(userId);

        return Optional.ofNullable(UserResponseDto.builder()
                .id(user.get().id())
                .email(profileByAccountId.getBody().email())
                .fullName(user.get().fullName())
                .imageLink(user.get().imageLink())
                .city(user.get().city())
                .reputation(user.get().reputation())
                .registrationDate(user.get().registrationDate())
                .votes(user.get().votes())
                .listTop3TagDto(tagDtoService.getTop3TagsByUserId(userId))
                .build());
    }
}