package stackover.resource.service.service.dto.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import stackover.resource.service.dto.response.QuestionBaseDto;
import stackover.resource.service.dto.response.QuestionResponseDto;
import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.repository.dto.QuestionDtoRepository;
import stackover.resource.service.service.dto.QuestionDtoService;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class QuestionDtoServiceImpl implements QuestionDtoService {

    private final QuestionDtoRepository questionDtoRepository;

    @Override
    public Optional<QuestionResponseDto> getQuestionDtoByQuestionIdAndUserId(Long questionId, Long userId) {
        Optional<QuestionBaseDto> questionBaseDto = questionDtoRepository.getQuestionBaseDtoByQuestionIdAndUserId(questionId, userId);

        return questionBaseDto.map(baseDto -> {
            List<TagResponseDto> tags = questionDtoRepository.findTagsByQuestionId(questionId);
            return new QuestionResponseDto(
                    baseDto.id(),
                    baseDto.title(),
                    baseDto.authorId(),
                    baseDto.authorName(),
                    baseDto.authorImage(),
                    baseDto.description(),
                    baseDto.viewCount(),
                    baseDto.authorReputation(),
                    baseDto.countAnswer(),
                    baseDto.countValuable(),
                    baseDto.persistDateTime(),
                    baseDto.lastUpdateDateTime(),
                    baseDto.countVote(),
                    baseDto.voteTypeQuestion(),
                    tags
            );
        });
    }
}
