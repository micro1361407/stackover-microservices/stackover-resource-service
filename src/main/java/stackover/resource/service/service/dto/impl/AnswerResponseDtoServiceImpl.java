package stackover.resource.service.service.dto.impl;

import org.springframework.stereotype.Service;

import stackover.resource.service.dto.response.AnswerResponseDto;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.repository.dto.AnswerResponseDtoRepository;
import stackover.resource.service.service.dto.AnswerResponseDtoService;


import java.util.Optional;

@Service
public class AnswerResponseDtoServiceImpl implements AnswerResponseDtoService {
    private final AnswerResponseDtoRepository answerResponseDto;

    public AnswerResponseDtoServiceImpl(AnswerResponseDtoRepository answerResponseDto) {
        this.answerResponseDto = answerResponseDto;
    }



    @Override
    public Optional<AnswerResponseDto> toDto(Answer answer) {
        var answerDtoOptional = answerResponseDto.toDto(answer);
        return answerDtoOptional.map(answerDto ->
                AnswerResponseDto.builder()
                        .id(answerDto.id())
                        .userId(answerDto.userId())
                        .questionId(answerDto.questionId())
                        .body(answerDto.body())
                        .persistDate(answerDto.persistDate())
                        .isHelpful(answerDto.isHelpful())
                        .dateAccept(answerDto.dateAccept())
                        .countValuable(answerDto.countValuable())
                        .countUserReputation(answerDto.countUserReputation())
                        .image(answerDto.image())
                        .nickname(answerDto.nickname())
                        .countVote(answerDto.countVote())
                        .voteTypeAnswer(answerDto.voteTypeAnswer())
                        .build()
        );
    }
}
