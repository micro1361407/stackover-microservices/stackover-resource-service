package stackover.resource.service.service.dto.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import stackover.resource.service.dto.response.CommentAnswerResponseDto;
import stackover.resource.service.entity.question.answer.CommentAnswer;
import stackover.resource.service.repository.dto.CommentAnswerDtoRepository;
import stackover.resource.service.service.dto.CommentDtoService;

import java.util.Optional;


@Service
@Slf4j
@RequiredArgsConstructor



public class CommentDtoServiceImpl implements CommentDtoService {

    private final CommentAnswerDtoRepository commentAnswerDtoRepository;

    @Override
    public Optional<CommentAnswerResponseDto> toDto(CommentAnswer commentAnswer) {
        var commentDtoOptional = commentAnswerDtoRepository.commentAnswerToDto(commentAnswer.getId());

        return commentDtoOptional.map(commentDto ->
                CommentAnswerResponseDto.builder()
                        .id(commentDto.id())
                        .answerId(commentDto.answerId())
                        .lastRedactionDate(commentDto.lastRedactionDate())
                        .persistDate(commentDto.persistDate())
                        .text(commentDto.text())
                        .userId(commentDto.userId())
                        .imageLink(commentDto.imageLink())
                        .reputation(commentDto.reputation())
                        .build()
        );
    }
}