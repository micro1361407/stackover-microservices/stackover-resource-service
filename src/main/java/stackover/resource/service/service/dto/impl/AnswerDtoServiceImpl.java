package stackover.resource.service.service.dto.impl;

import org.springframework.stereotype.Service;
import stackover.resource.service.dto.response.AnswerResponseDto;
import stackover.resource.service.repository.dto.AnswerDtoRepository;
import stackover.resource.service.service.dto.AnswerDtoService;

import java.util.List;

@Service
public class AnswerDtoServiceImpl implements AnswerDtoService {

    private final AnswerDtoRepository answerDtoRepository;

    public AnswerDtoServiceImpl(AnswerDtoRepository answerDtoRepository) {
        this.answerDtoRepository = answerDtoRepository;
    }

    @Override
    public List<AnswerResponseDto> getAnswersDtoByQuestionId(Long questionId, Long accouuntId) {
        return answerDtoRepository.getAnswersDtoByQuestionId(questionId, accouuntId);
    }
}
