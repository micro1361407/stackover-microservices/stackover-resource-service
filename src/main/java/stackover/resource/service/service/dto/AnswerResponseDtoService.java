package stackover.resource.service.service.dto;

import stackover.resource.service.dto.response.AnswerResponseDto;
import stackover.resource.service.entity.question.answer.Answer;

import java.util.List;
import java.util.Optional;

public interface AnswerResponseDtoService {
    Optional<AnswerResponseDto> toDto(Answer answer);

}