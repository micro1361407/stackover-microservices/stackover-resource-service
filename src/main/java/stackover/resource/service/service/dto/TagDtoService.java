package stackover.resource.service.service.dto;

import stackover.resource.service.dto.response.TagResponseDto;

import java.util.List;


public interface TagDtoService {
    List<TagResponseDto> getTop3TagsByUserId(Long accountId);
}
