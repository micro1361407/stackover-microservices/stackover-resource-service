package stackover.resource.service.service.dto;


import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.entity.question.IgnoredTag;

import java.util.Optional;

public interface IgnoredTagDtoService {
    Optional<TagResponseDto> toTagResponseDto(IgnoredTag ignoredTag);
}