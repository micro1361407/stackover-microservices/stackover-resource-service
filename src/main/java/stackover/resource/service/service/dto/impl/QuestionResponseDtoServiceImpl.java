package stackover.resource.service.service.dto.impl;


import org.springframework.stereotype.Service;
import stackover.resource.service.dto.response.QuestionResponseDto;
import stackover.resource.service.entity.question.Question;
import stackover.resource.service.repository.dto.QuestionResponseDtoRepository;
import stackover.resource.service.service.dto.QuestionResponseDtoService;
import stackover.resource.service.service.dto.TagDtoService;

import java.util.Optional;

@Service
public class QuestionResponseDtoServiceImpl implements QuestionResponseDtoService {

    private final QuestionResponseDtoRepository questionResponseDto;
    private final TagDtoService tagDTOService;

    public QuestionResponseDtoServiceImpl(QuestionResponseDtoRepository questionResponseDto, TagDtoService tagDtoService) {
        this.questionResponseDto = questionResponseDto;
        this.tagDTOService = tagDtoService;
    }

    @Override
    public Optional<QuestionResponseDto> toDto(Question question) {
        var questionDtoOptional = questionResponseDto.toDto(question);

        return questionDtoOptional.map(questionDto ->
                QuestionResponseDto.builder()
                        .id(questionDto.id())
                        .title(questionDto.title())
                        .authorId(questionDto.authorId())
                        .authorName(questionDto.authorName())
                        .authorImage(questionDto.authorImage())
                        .description(questionDto.description())
                        .viewCount(questionDto.viewCount())
                        .authorReputation(questionDto.authorReputation())
                        .countAnswer(questionDto.countAnswer())
                        .countValuable(questionDto.countValuable())
                        .persistDateTime(questionDto.persistDateTime())
                        .lastUpdateDateTime(questionDto.lastUpdateDateTime())
                        .countVote(questionDto.countVote())
                        .voteTypeQuestion(questionDto.voteTypeQuestion())
                        .listTagDto(tagDTOService.getTop3TagsByUserId(questionDto.authorId()))
                        .build()
        );
    }
}
