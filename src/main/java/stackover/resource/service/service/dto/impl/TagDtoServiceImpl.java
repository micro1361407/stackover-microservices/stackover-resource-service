package stackover.resource.service.service.dto.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.feign.AuthServiceFeignClient;
import stackover.resource.service.repository.dto.TagDtoRepository;
import stackover.resource.service.service.dto.TagDtoService;

import java.util.Collections;
import java.util.List;


@Slf4j
@Service
public class TagDtoServiceImpl implements TagDtoService {

    private final TagDtoRepository tagDtoRepository;
    private final AuthServiceFeignClient authServiceFeignClient;


    public TagDtoServiceImpl(TagDtoRepository tagDtoRepository, AuthServiceFeignClient authServiceFeignClient) {
        this.tagDtoRepository = tagDtoRepository;
        this.authServiceFeignClient = authServiceFeignClient;
    }

    @Override
    public List<TagResponseDto> getTop3TagsByUserId(Long accountId) {
        if (!authServiceFeignClient.isAccountExist(accountId)) {
            log.error("Account with ID {} does not exist", accountId);
            return Collections.emptyList();
        }
        return tagDtoRepository.getTop3TagsByUserId(accountId);
    }
}
