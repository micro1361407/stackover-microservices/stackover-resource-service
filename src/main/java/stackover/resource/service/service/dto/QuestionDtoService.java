package stackover.resource.service.service.dto;

import stackover.resource.service.dto.response.QuestionResponseDto;

import java.util.Optional;

public interface QuestionDtoService {

    Optional<QuestionResponseDto> getQuestionDtoByQuestionIdAndUserId(Long questionId, Long userId);
}
