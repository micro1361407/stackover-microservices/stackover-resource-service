package stackover.resource.service.service;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.entity.user.reputation.Reputation;
import stackover.resource.service.service.entity.AbstractService;

import java.time.LocalDateTime;
import java.util.Optional;
@Service
public interface ReputationService extends AbstractService<Reputation, Long> {

    Optional<Reputation>  findByAnswerAndAndAuthor(Answer answer, User user);


}
