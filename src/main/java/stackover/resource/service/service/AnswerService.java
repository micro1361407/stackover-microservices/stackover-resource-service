package stackover.resource.service.service;

import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.service.entity.AbstractService;

public interface AnswerService extends AbstractService<Answer, Long> {
}
