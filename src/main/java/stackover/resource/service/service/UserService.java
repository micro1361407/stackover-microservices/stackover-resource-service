package stackover.resource.service.service;

import stackover.resource.service.entity.user.User;
import stackover.resource.service.service.entity.AbstractService;

public interface UserService extends AbstractService<User, Long> {

}