package stackover.resource.service.service;

import stackover.resource.service.entity.question.Question;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.service.entity.AbstractService;

public interface QuestionService extends AbstractService<Question, Long> {
}
