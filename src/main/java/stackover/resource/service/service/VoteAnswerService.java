package stackover.resource.service.service;

import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.question.answer.VoteAnswer;
import stackover.resource.service.entity.user.User;
import stackover.resource.service.service.entity.AbstractService;

import java.util.Optional;

public interface VoteAnswerService extends AbstractService<VoteAnswer, Long> {
    Optional<VoteAnswer> findByAnswerAndUser(Answer answer, User user);
    Long countByAnswerId(Long answerId);
}
