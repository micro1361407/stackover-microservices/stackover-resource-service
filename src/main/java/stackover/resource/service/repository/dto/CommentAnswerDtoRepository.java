package stackover.resource.service.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.response.CommentAnswerResponseDto;
import stackover.resource.service.entity.question.answer.CommentAnswer;

import java.util.Optional;


@Repository
public interface CommentAnswerDtoRepository extends JpaRepository<CommentAnswer, Long> {
    @Query("""
            SELECT new stackover.resource.service.dto.response.CommentAnswerResponseDto(
                ca.id,
                a.id,
                ct.lastUpdateDateTime,
                ct.persistDateTime,
                ct.text,
                u.id,
                u.imageLink,
                (SELECT COALESCE(SUM(r.count), 0) FROM Reputation r WHERE r.author.id = u.id)
            )
            FROM CommentAnswer ca
            JOIN ca.commentText ct
            JOIN ct.user u
            JOIN ca.answer a
            WHERE ca.id = :answerId
            """)
    Optional<CommentAnswerResponseDto> commentAnswerToDto(@Param("answerId") Long answerId);
}

