package stackover.resource.service.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.entity.question.Tag;

import java.util.List;

@Repository
public interface TagDtoRepository extends JpaRepository<Tag, Long> {

    @Query(value = """
            SELECT new stackover.resource.service.dto.response.TagResponseDto (
            t.id,
            t.name,
            t.description)
            FROM Tag t
            JOIN t.questions q
            LEFT JOIN Reputation r ON r.question.id = q.id AND r.type = stackover.resource.service.entity.user.reputation.ReputationType.VOTE_ANSWER
            WHERE q.user.accountId = :accountId
            GROUP BY t.id, t.name, t.description
            ORDER BY COUNT(t.name) DESC
            LIMIT 3
            """)
    List<TagResponseDto> getTop3TagsByUserId(@Param("accountId") Long accountId);
}
