package stackover.resource.service.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.response.AnswerResponseDto;
import stackover.resource.service.entity.question.answer.Answer;

import java.util.Optional;

@Repository
public interface AnswerResponseDtoRepository extends JpaRepository<Answer, Long> {
    @Query
            ("""
                     SELECT NEW stackover.resource.service.dto.response.AnswerResponseDto(
                         answer.id,
                         answer.user.id,
                         answer.question.id,
                         answer.htmlBody,
                         answer.persistDateTime,
                         answer.isHelpful,
                         answer.dateAcceptTime,
                         COALESCE((SELECT SUM(r.count) FROM Reputation r WHERE r.answer.id = answer.id), 0) ,
                         COALESCE((SELECT SUM(r.count) FROM Reputation r WHERE r.author.id = answer.user.id), 0) ,
                         answer.user.imageLink,
                         answer.user.nickname,
                         (SELECT COALESCE(SUM(CASE WHEN v.voteTypeAnswer = 'UP' THEN 1 WHEN v.voteTypeAnswer = 'DOWN' THEN 1 ELSE 0 END ), 0)
                          FROM VoteAnswer v WHERE v.answer.id = answer.id),
                         (SELECT v.voteTypeAnswer FROM VoteAnswer v WHERE v.answer.id = answer.id AND v.user.id = :userId)
                     )
                     FROM Answer answer
                     WHERE answer = :answer
                     """)
    Optional<AnswerResponseDto> toDto(Answer answer);}