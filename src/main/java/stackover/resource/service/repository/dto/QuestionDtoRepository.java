package stackover.resource.service.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.response.QuestionBaseDto;
import stackover.resource.service.dto.response.QuestionResponseDto;
import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.entity.question.Question;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuestionDtoRepository extends JpaRepository<Question, Long> {
    @Query(value = """
            SELECT new stackover.resource.service.dto.response.QuestionResponseDto(
                q.id,
                q.title,
                u.id,
                u.fullName,
                u.imageLink,
                q.description,
                (SELECT COUNT(vw) FROM QuestionViewed vw WHERE vw.question.id = q.id),
                (SELECT COALESCE(SUM(r.count), 0) FROM Reputation r WHERE r.author.id = u.id),
                (SELECT COUNT(a) FROM Answer a WHERE a.question.id = q.id),
                (SELECT COALESCE(SUM(CASE vq.voteTypeQuestion WHEN stackover.resource.service.entity.question.VoteTypeQuestion.UP THEN 1 WHEN stackover.resource.service.entity.question.VoteTypeQuestion.DOWN THEN -1 ELSE 0 END), 0) FROM VoteQuestion vq WHERE vq.question.id = q.id),
                q.persistDateTime,
                q.lastUpdateDateTime,
                (SELECT COUNT(vq) FROM VoteQuestion vq WHERE vq.question.id = q.id),
                (SELECT vq.voteTypeQuestion FROM VoteQuestion vq WHERE vq.question.id = q.id AND vq.user.id = q.user.id)
            )
            FROM Question q
            JOIN q.user u
            LEFT JOIN Answer a ON a.question.id = q.id
            LEFT JOIN VoteQuestion vq ON vq.question.id = q.id
            LEFT JOIN q.tags tags
            WHERE q = :question
            """)
    Optional<QuestionResponseDto> toDto(Question question);

    @Query("""
            SELECT new stackover.resource.service.dto.response.QuestionBaseDto(
                q.id, q.title,
                q.user.id, q.user.fullName, q.user.imageLink,
                q.description,
                (SELECT COUNT(qv.id) FROM QuestionViewed qv WHERE qv.question.id = q.id),
                (SELECT SUM(r.count) FROM Reputation r WHERE r.author.id = q.user.id),
                (SELECT COUNT(a.id) FROM Answer a WHERE a.question.id = q.id),
                (SELECT
                    COALESCE(SUM(CASE WHEN v.voteTypeQuestion = 'UP' THEN 1 ELSE 0 END), 0) -
                    COALESCE(SUM(CASE WHEN v.voteTypeQuestion = 'DOWN' THEN 1 ELSE 0 END), 0)
                    FROM VoteQuestion v WHERE v.question.id = q.id),
                q.persistDateTime, q.lastUpdateDateTime,
                (SELECT COUNT(v.id) FROM VoteQuestion v WHERE v.question.id = q.id),
                (SELECT v.voteTypeQuestion FROM VoteQuestion v WHERE v.question.id = q.id AND v.user.id = :userId)
                )
            FROM Question q
            WHERE q.id = :questionId AND q.user.id = :userId
            """)
    Optional<QuestionBaseDto> getQuestionBaseDtoByQuestionIdAndUserId(Long questionId, Long userId);

    @Query("""
            SELECT new stackover.resource.service.dto.response.TagResponseDto(
                t.id, t.name, t.description
                )
            FROM Question q JOIN q.tags t WHERE q.id = :questionId
            """)
    List<TagResponseDto> findTagsByQuestionId(Long questionId);
}