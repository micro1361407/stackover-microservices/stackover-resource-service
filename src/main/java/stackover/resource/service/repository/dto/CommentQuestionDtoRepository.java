package stackover.resource.service.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.response.CommentQuestionResponseDto;
import stackover.resource.service.entity.question.CommentQuestion;

import java.util.List;

@Repository
public interface CommentQuestionDtoRepository extends JpaRepository<CommentQuestion, Long> {

    @Query("""
        SELECT new stackover.resource.service.dto.response.CommentQuestionResponseDto(
            cq.id,
            q.id,
            ct.lastUpdateDateTime,
            ct.persistDateTime,
            ct.text,
            u.id,
            u.imageLink,
            (SELECT COALESCE(SUM(r.count), 0) FROM Reputation r WHERE r.author.id = u.id)
        )
        FROM CommentQuestion cq
            JOIN cq.question q
            JOIN cq.commentText ct
            JOIN User u ON ct.user.id = u.id
            WHERE q.id = :questionId
            ORDER BY ct.persistDateTime DESC
    """)
    List<CommentQuestionResponseDto> getAllQuestionCommentDtoById(@Param("questionId") Long questionId);
}
