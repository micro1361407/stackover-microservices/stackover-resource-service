package stackover.resource.service.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.response.QuestionResponseDto;
import stackover.resource.service.entity.question.Question;

import java.util.Optional;


@Repository
public interface QuestionResponseDtoRepository extends JpaRepository<Question, Long> {
    @Query(value = """
            SELECT new stackover.resource.service.dto.response.QuestionResponseDto(
                q.id,
                q.title,
                u.id,
                u.fullName,
                u.imageLink,
                q.description,
                (SELECT COUNT(vw) FROM QuestionViewed vw WHERE vw.question.id = q.id),
                (SELECT COALESCE(SUM(r.count), 0) FROM Reputation r WHERE r.author.id = u.id),
                (SELECT COUNT(a) FROM Answer a WHERE a.question.id = q.id),
                (SELECT COALESCE(SUM(CASE vq.voteTypeQuestion WHEN stackover.resource.service.entity.question.VoteTypeQuestion.UP THEN 1 WHEN stackover.resource.service.entity.question.VoteTypeQuestion.DOWN THEN -1 ELSE 0 END), 0) FROM VoteQuestion vq WHERE vq.question.id = q.id),
                q.persistDateTime,
                q.lastUpdateDateTime,
                (SELECT COUNT(vq) FROM VoteQuestion vq WHERE vq.question.id = q.id),
                (SELECT vq.voteTypeQuestion FROM VoteQuestion vq WHERE vq.question.id = q.id AND vq.user.id = q.user.id)
            ) 
            FROM Question q
            JOIN q.user u 
            LEFT JOIN Answer a ON a.question.id = q.id
            LEFT JOIN VoteQuestion vq ON vq.question.id = q.id
            LEFT JOIN q.tags tags
            WHERE q = :question
            """)
    Optional<QuestionResponseDto> toDto(Question question);

    @Query("""
                SELECT NEW stackover.resource.service.dto.response.QuestionResponseDto (
                    question.id,
                    question.title,
                    question.user.id,
                    question.user.fullName,
                    question.user.imageLink,
                    question.description,
                    (select coalesce(count(qv.id), 0) from QuestionViewed qv where qv.question = question),
                    (select coalesce(sum(r.count), 0) from Reputation r where r.author = question.user),
                    (select coalesce(count(a.id), 0) from Answer a where a.question = question),
                    (select coalesce(sum(case when vq.voteTypeQuestion = stackover.resource.service.entity.question.VoteTypeQuestion.UP then 1
                                   when vq.voteTypeQuestion = stackover.resource.service.entity.question.VoteTypeQuestion.DOWN then (0-1)
                                   else 0 end), 0) from VoteQuestion vq where vq.question = question),
                    question.persistDateTime,
                    question.lastUpdateDateTime,
                    (select coalesce(count(vq.id), 0) from VoteQuestion vq where vq.question = question),
                    (select voq.voteTypeQuestion
                        from VoteQuestion voq
                        where voq.question.id = question.id and voq.user.accountId = :userAccountId)
                ) from Question question
                where question.id = :questionId
""")
    Optional<QuestionResponseDto> getQuestionResponseDtoByQuestionIdAndUserId(@Param("questionId") Long questionId, @Param("userAccountId") Long userAccountId);
}