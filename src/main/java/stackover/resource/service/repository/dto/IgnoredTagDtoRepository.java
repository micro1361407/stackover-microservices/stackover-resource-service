package stackover.resource.service.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.entity.question.IgnoredTag;

import java.util.Optional;

@Repository
public interface IgnoredTagDtoRepository extends JpaRepository<IgnoredTag, Long> {

    @Query(value = """
            SELECT new stackover.resource.service.dto.response.TagResponseDto (
                t.id,
                t.name,
                t.description
            )
            FROM Tag t
            WHERE EXISTS (
                SELECT it.ignoredTag
                FROM IgnoredTag it
                WHERE it.ignoredTag.id = t.id AND it.ignoredTag.id = :ignoredTagId AND it.user.id = :userId
            )
            """)
    Optional<TagResponseDto> toTagResponseDto(@Param("ignoredTagId") Long ignoredTagId, @Param("userId") Long userId);
}
