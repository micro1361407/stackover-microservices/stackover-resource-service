package stackover.resource.service.repository.dto;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import stackover.resource.service.dto.response.UserResponseDto;
import stackover.resource.service.entity.user.User;

import java.util.Optional;

@Repository
public interface UserDtoRepository extends JpaRepository<User, Long> {

    @Query(value = """
            SELECT new stackover.resource.service.dto.response.UserResponseDto(
                user.id,
                user.fullName,
                user.imageLink,
                user.city,
                (SELECT COUNT(DISTINCT reputation.id) FROM Reputation reputation WHERE reputation.author.id = :userId),
                user.persistDateTime,
                ((SELECT COUNT(DISTINCT answer.id) FROM VoteAnswer answer WHERE answer.user.id = :userId) +
                (SELECT COUNT(DISTINCT question.id) FROM VoteQuestion question WHERE question.user.id = :userId))
            ) FROM User user
            WHERE user.id = :userId
            """)
    Optional<UserResponseDto> getUserDtoByUserId(@Param("userId") Long userId);
}
