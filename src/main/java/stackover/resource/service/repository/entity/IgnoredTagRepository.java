package stackover.resource.service.repository.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import stackover.resource.service.entity.question.IgnoredTag;

@Repository
public interface IgnoredTagRepository extends JpaRepository<IgnoredTag, Long> {
}
