package stackover.resource.service.converters;


import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Converter
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, String> {

    @Override
    public String convertToDatabaseColumn(LocalDateTime localDateTime) {
        if (localDateTime == null) {

            return null;
        }
        LocalDateTime startOfDay = localDateTime.toLocalDate().atStartOfDay();

        return startOfDay.toString();
    }

    @Override
    public LocalDateTime convertToEntityAttribute(String timestampString) {
        if (timestampString == null) {

            return null;

        } else {

            return LocalDateTime.parse(timestampString);
        }

    }
}

