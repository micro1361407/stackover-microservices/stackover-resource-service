package stackover.resource.service.converters;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.entity.question.Tag;
import stackover.resource.service.repository.entity.TagRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
public class TagConverter {

    private final TagRepository tagRepository;



    public TagConverter(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public List<TagResponseDto> tagListToTagResponse(List<Tag> tagList) {

        return tagList.stream()
                .map(tag -> new TagResponseDto(
                        tag.getId(),
                        tag.getName(),
                        tag.getDescription()
                )).collect(Collectors.toList());
    }
    public List<Tag> tagResponseToTagList(List<TagResponseDto> tagResponseDtoList) {
        return tagResponseDtoList.stream().map(tagResponseDto -> {
            Optional<Tag> existTag = tagRepository.findById(tagResponseDto.id());
            if (existTag.isPresent()) {
                return existTag.get();
            } else {
                Tag newTag = new Tag();
                newTag.setName(tagResponseDto.name());
                newTag.setDescription(tagResponseDto.description());
                return tagRepository.save(newTag);
            }
        }).collect(Collectors.toList());
    }
}
