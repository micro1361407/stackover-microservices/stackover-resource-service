package stackover.resource.service.converters;

import org.springframework.stereotype.Component;
import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.user.User;

@Component
public class AnswerConverter {
    public Answer toAnswer(AnswerRequestDto answerRequestDto, User user) {
        Answer answer = new Answer();
        answer.setHtmlBody(answerRequestDto.body());
        answer.setUser(user);
        return answer;
    }
}



