package stackover.resource.service.converters;

import org.springframework.stereotype.Component;
import stackover.resource.service.dto.request.QuestionCreateRequestDto;
import stackover.resource.service.dto.response.QuestionResponseDto;
import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.entity.question.Question;
import stackover.resource.service.entity.question.Tag;
import stackover.resource.service.entity.user.User;


import java.util.List;


@Component
public class QuestionConverter {
    private final TagConverter tagConverter;
    public QuestionConverter(TagConverter tagConverter) {
        this.tagConverter = tagConverter;
    }

    public Question toEntity(QuestionCreateRequestDto questionCreateRequestDto, User user) {
        Question question = new Question();
        question.setTitle(questionCreateRequestDto.title());
        question.setDescription(questionCreateRequestDto.description());


        List<TagResponseDto> tagResponseDtoList = questionCreateRequestDto.tags();
        if (tagResponseDtoList != null && !tagResponseDtoList.isEmpty()) {
            List<Tag> tags = tagConverter.tagResponseToTagList(tagResponseDtoList);
            question.setTags(tags);
        }
        question.setUser(user);
        return question;
    }

    public QuestionResponseDto toDto(Question question) {
        return QuestionResponseDto.builder()
                .authorId(question.getUser().getId())
                .authorName(question.getUser().getFullName())
                .listTagDto(tagConverter.tagListToTagResponse(question.getTags()))
                .build();
    }
}
