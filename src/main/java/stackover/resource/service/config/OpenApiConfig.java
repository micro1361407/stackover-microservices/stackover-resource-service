package stackover.resource.service.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI getOpenApiConfig() {
        Contact contact = new Contact();
        contact.setName("StackOver KATA Team");
        contact.setUrl("https://Kata.Academy");

        Info info = new Info();
        info.setTitle("StackOver Resource Service");
        info.setDescription("Документация Rest API для контроллера ResourceUserController. Project TB Group 04.2024");
        info.setContact(contact);
        info.version("0.0.1");

        return new OpenAPI().info(info);
    }
}
