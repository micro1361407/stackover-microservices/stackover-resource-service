package stackover.resource.service.feign;


import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Component;
import stackover.resource.service.dto.response.AccountResponseDto;
//TODO Тестовый тестовый пользователь из auth-service
@Component
public class AuthServiceFeignClientStub implements AuthServiceFeignClient {
    @Override
    public Boolean isAccountExist(Long accountId) {
        return true;
    }

    @Override
    public ResponseEntity<AccountResponseDto> getAccountById(Long accountId) {
        AccountResponseDto accountResponseDto = AccountResponseDto.builder()
                .id(accountId)
                .email("stub@example.com")
                .build();
        return ResponseEntity.ok(accountResponseDto);
    }
}
