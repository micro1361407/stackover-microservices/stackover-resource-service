package stackover.resource.service.feign;

import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import stackover.resource.service.dto.request.ProfileRequestDto;
import stackover.resource.service.exception.ApiRequestException;

@FeignClient(value = "stackover-profile-service", fallback = ProfileServiceFeignClient.ProfileServiceFallbackFactory.class,
        path = "/api/inner/profile")
public interface ProfileServiceFeignClient {

    @GetMapping("/{account_id}")
    ResponseEntity<ProfileRequestDto> getProfileByAccountId(@PathVariable("account_id") Long accountId);

    @Component
    class ProfileServiceFallbackFactory implements FallbackFactory<FallbackWithFactory> {

        @Override
        public FallbackWithFactory create(Throwable cause) {
            return new FallbackWithFactory(cause.getMessage());
        }
    }

    record FallbackWithFactory(String reason) implements ProfileServiceFeignClient {
        @Override
        public ResponseEntity<ProfileRequestDto> getProfileByAccountId(Long id) {
            String responseMessage = """
                    Profile with id %s not found
                    """.formatted(id);
            throw new ApiRequestException(responseMessage);
        }
    }
}
