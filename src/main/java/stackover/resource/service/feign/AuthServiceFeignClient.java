package stackover.resource.service.feign;

import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import stackover.resource.service.dto.response.AccountResponseDto;
import stackover.resource.service.exception.AccountExistException;

@FeignClient(name = "stackover-auth-service", fallbackFactory = AuthServiceFeignClient.AuthServiceFallbackFactory.class,
        path = "/api/internal/account")
public interface AuthServiceFeignClient {

    @GetMapping("/{accountId}/exist")
    Boolean isAccountExist(@PathVariable Long accountId);


    @GetMapping("/{accountId}")
    ResponseEntity<AccountResponseDto> getAccountById(@PathVariable Long accountId);

    @Component
    class AuthServiceFallbackFactory implements FallbackFactory<FallbackWithFactory> {
        @Override
        public FallbackWithFactory create(Throwable cause) {
            return new FallbackWithFactory(cause.getMessage());
        }
    }

    record FallbackWithFactory(String reason) implements AuthServiceFeignClient {
        @Override
        public Boolean isAccountExist(@PathVariable Long accountId) {
            String responseMessage = """
                    Account with id %s not found
                    """.formatted(accountId);
            throw new AccountExistException(responseMessage);
        }

        @Override
        public ResponseEntity<AccountResponseDto> getAccountById(Long accountId) {
            throw new AccountExistException(String.format("Account with id %s not found", accountId));
        }
    }
}
