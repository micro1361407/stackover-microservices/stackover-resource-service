package stackover.resource.service.exception.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import stackover.resource.service.exception.AccountExistException;

@RestControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(value = AccountExistException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<String> handleAccountExistException(RuntimeException e) {
        return new  ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }
}
