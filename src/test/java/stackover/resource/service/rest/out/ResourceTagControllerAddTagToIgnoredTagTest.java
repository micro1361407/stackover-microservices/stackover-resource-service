package stackover.resource.service.rest.out;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import stackover.resource.service.SpringSimpleContextTest;
import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.feign.AuthServiceFeignClient;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@MockBeans({
        @MockBean(AuthServiceFeignClient.class),
})
@TestPropertySource(properties = {
        "spring.config.location=classpath:application-ResourceTagControllerAddTagToIgnoredTagTest.yml"
})
@DisplayName("Тест: Добавление тега в игнорируемые теги пользователя")
public class ResourceTagControllerAddTagToIgnoredTagTest extends SpringSimpleContextTest {

    @Autowired
    private AuthServiceFeignClient authServiceFeignClient;

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            value = "/scripts/out/ResourceTagControllerTest/addTagToIgnoredTagTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
            value = "/scripts/out/ResourceTagControllerTest/addTagToIgnoredTagTest/AfterTest.sql")
    @DisplayName("Позитивный: добавляемый тег отсутствует в игнорируемых тегах пользователя")
    public void addTagToIgnoredTag_PositiveTest() throws Exception {
        Long accountId = 1001L;
        Long tagId = 101L;
        String tagName = "Tag 1";
        String tagDescription = "Description 1";

        when(authServiceFeignClient.isAccountExist(accountId)).thenReturn(true);

        mockMvc.perform(post("/api/user/tag/" + tagId + "/ignored?accountId=" + accountId))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(tagId.intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", is(tagName)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description", is(tagDescription)));
    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            value = "/scripts/out/ResourceTagControllerTest/addTagToIgnoredTagTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
            value = "/scripts/out/ResourceTagControllerTest/addTagToIgnoredTagTest/AfterTest.sql")
    @DisplayName("Негативный: добавление ")
    public void addTagToIgnoredTag_NegativeTest() throws Exception {
        Long accountId = 1002L;
        Long userId = 101L;
        Long tagId = 103L;
        String tagName = "Tag 1";
        String tagDescription = "Description 1";

        TagResponseDto tagResponseDTO = TagResponseDto.builder()
                .id(tagId)
                .name(tagName)
                .description(tagDescription)
                .build();

        when(authServiceFeignClient.isAccountExist(accountId)).thenReturn(true);

        mockMvc.perform(post("/api/user/tag/" + tagId + "/ignored?accountId=" + accountId))
                .andExpect(status().isNotFound());
    }
}
