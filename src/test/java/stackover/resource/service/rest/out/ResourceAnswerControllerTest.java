package stackover.resource.service.rest.out;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import stackover.resource.service.SpringSimpleContextTest;
import stackover.resource.service.dto.request.AnswerRequestDto;
import stackover.resource.service.dto.response.AnswerResponseDto;
import stackover.resource.service.entity.question.answer.Answer;
import stackover.resource.service.entity.question.answer.VoteTypeAnswer;
import stackover.resource.service.feign.AuthServiceFeignClient;
import stackover.resource.service.feign.ProfileServiceFeignClient;
import stackover.resource.service.service.dto.AnswerResponseDtoService;
import stackover.resource.service.service.entity.AnswerService;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@MockBeans({
        @MockBean(AuthServiceFeignClient.class),
        @MockBean(ProfileServiceFeignClient.class),
        @MockBean(AnswerService.class),
        @MockBean(AnswerResponseDtoService.class)
})
@TestPropertySource(locations = "classpath:application-test.yml")
@AutoConfigureMockMvc
@Slf4j
class ResourceAnswerControllerTest extends SpringSimpleContextTest {

    @Autowired
    private AuthServiceFeignClient authServiceFeignClient;

    @Autowired
    private AnswerService answerService;

    @Autowired
    private AnswerResponseDtoService answerResponseDtoService;

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/addDownVoteAnswer/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/addDownVoteAnswer/AfterTest.sql")
    @DisplayName("Позитивный:  УСПЕШНО")
    void addDownVoteToAnswer() throws Exception {
        Long questionId = 1010L;
        Long answerId = 1020L;
        Long accountId = 102L; // Пользователь, который не является владельцем ответа

        log.info("Запуск теста с questionId={}, answerId={}, accountId={}", questionId, answerId, accountId);

        when(authServiceFeignClient.isAccountExist(accountId)).thenReturn(true);

        mockMvc.perform(post("/api/user/question/" + questionId + "/answer/" + answerId + "/downVote?accountId=" + accountId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("1"));

        log.info("Тест успешно завершён");

    }

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceAnswerControllerTest/updateAnswerBody/AfterTest.sql")
    void updateAnswerBody_updated() throws Exception {
        Long questionId = 101L;
        Long answerId = 101L;
        Long accountId = 1001L;

        Answer answer = new Answer();
        answer.setId(answerId);
        answer.setHtmlBody("Новое тело ответа");

        AnswerRequestDto requestDto = new AnswerRequestDto(
                101L,
                102L,
                101L,
                "Новое тело ответа",
                LocalDateTime.parse("2023-09-20T11:30"),
                false,
                LocalDateTime.parse("2023-09-20T12:00"),
                2L,
                2L,
                "image",
                "nickname",
                2L,
                VoteTypeAnswer.UP
        );
        AnswerResponseDto responseDto = new AnswerResponseDto(
                101L,
                102L,
                101L,
                "Новое тело ответа",
                LocalDateTime.parse("2023-09-20T11:30"),
                false,
                LocalDateTime.parse("2023-09-20T12:00"),
                2L,
                2L,
                "image",
                "nickname",
                2L,
                VoteTypeAnswer.UP
        );
        when(answerService.updateAnswer(answerId, questionId, requestDto, accountId)).thenReturn(Optional.of(answer));
        when(answerResponseDtoService.toDto(answer)).thenReturn(Optional.of(responseDto));

        mockMvc.perform(put("/api/user/question/{questionId}/answer/{answerId}/body", questionId, answerId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(responseDto))
                        .param("accountId", String.valueOf(accountId)))

                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.body").value("Новое тело ответа"));

    }

    @Test
    void updateAnswerBody_notFound() throws Exception {
        // Подготовка тестовых данных
        long questionId = 101L;
        long answerId = 101L;
        long accountId = 1L;

        AnswerRequestDto requestDto = new AnswerRequestDto(
                101L,
                1L,
                101L,
                "Новое тело ответа",
                LocalDateTime.parse("2023-09-20T11:30"),
                false,
                LocalDateTime.parse("2023-09-20T12:00"),
                2L,
                2L,
                "image",
                "nickname",
                2L,
                VoteTypeAnswer.UP
        );

        when(answerService.updateAnswer(answerId, questionId, requestDto, accountId)).thenReturn(Optional.empty());

        // Выполнение запроса
        mockMvc.perform(put("/api/user/question/{questionId}/answer/{answerId}/body", questionId, answerId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(requestDto))
                        .param("accountId", String.valueOf(accountId)))

                .andExpect(status().isNotFound());
    }
}



