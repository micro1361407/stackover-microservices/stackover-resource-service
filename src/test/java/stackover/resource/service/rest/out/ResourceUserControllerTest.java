package stackover.resource.service.rest.out;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import stackover.resource.service.SpringSimpleContextTest;
import stackover.resource.service.dto.request.ProfileRequestDto;
import stackover.resource.service.dto.response.AccountResponseDto;
import stackover.resource.service.feign.AuthServiceFeignClient;
import stackover.resource.service.feign.ProfileServiceFeignClient;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@MockBeans({
        @MockBean(AuthServiceFeignClient.class),
        @MockBean(ProfileServiceFeignClient.class)
})
@TestPropertySource(locations = "classpath:application-test.yml")
@AutoConfigureMockMvc
public class ResourceUserControllerTest extends SpringSimpleContextTest {

    @Autowired
    private AuthServiceFeignClient authServiceFeignClient;

    @Autowired
    private ProfileServiceFeignClient profileServiceFeignClient;

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
            value = "/scripts/out/ResourceUserControllerTest/getUserDtoByUserId/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
            value = "/scripts/out/ResourceUserControllerTest/getUserDtoByUserId/AfterTest.sql")
    public void getUserDtoByUserIdIfAccountExist() throws Exception {

        Long userId = 101L;
        Long reputation = 3L;
        Long votes = 2L;

        AccountResponseDto accountResponseDto = AccountResponseDto
                .builder()
                .id(userId)
                .email("email@email.com")
                .build();

        ProfileRequestDto profileRequestDto = ProfileRequestDto
                .builder()
                .accountId(userId)
                .fullName("fullName")
                .email("email@email.com")
                .city("city")
                .about("about")
                .imageLink("imageLink")
                .linkVk("linkVk")
                .linkSite("linkSite")
                .linkGitHub("linkGitHub")
                .nickname("nickname")
                .build();

        when(authServiceFeignClient.isAccountExist(userId)).thenReturn(true);
        when(authServiceFeignClient.getAccountById(userId)).thenReturn(ResponseEntity.ok(accountResponseDto));
        when(profileServiceFeignClient.getProfileByAccountId(userId)).thenReturn(ResponseEntity.ok(profileRequestDto));

        mockMvc.perform(get("/api/user/{userId}", userId))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(userId.intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.fullName", is("John Doe")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", is("email@email.com")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.reputation", is(reputation.intValue())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.votes", is(votes.intValue())));
    }

    @Test
    public void getUserDtoByUserId_WhenAccountDoesNotExist() throws Exception {

        Long userId = -1L;

        when(authServiceFeignClient.isAccountExist(userId)).thenReturn(false);

        mockMvc.perform(get("/api/user/{userId}", userId))
                .andExpect(status().isNotFound());

    }
}