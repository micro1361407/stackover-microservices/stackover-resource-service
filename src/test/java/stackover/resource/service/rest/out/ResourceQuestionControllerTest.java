package stackover.resource.service.rest.out;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.mockito.Mockito.*;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import stackover.resource.service.SpringSimpleContextTest;
import stackover.resource.service.dto.request.QuestionCreateRequestDto;
import stackover.resource.service.dto.response.TagResponseDto;
import stackover.resource.service.feign.AuthServiceFeignClient;
import stackover.resource.service.feign.ProfileServiceFeignClient;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

@RunWith(SpringRunner.class)
@MockBeans({
        @MockBean(AuthServiceFeignClient.class),
        @MockBean(ProfileServiceFeignClient.class)
})
@TestPropertySource(locations = "classpath:application-test.yml")
@AutoConfigureMockMvc
@Slf4j
public class ResourceQuestionControllerTest extends SpringSimpleContextTest {

    @Autowired
    private MockMvc mockMvc;

    @Qualifier("stackover.resource.service.feign.AuthServiceFeignClient")
    @Autowired
    private AuthServiceFeignClient authServiceFeignClient;

    @Autowired
    private DataSource dataSource;

    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, value = "/scripts/out/ResourceQuestionControllerTest/addNewQuestionTrueSolvedTest/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, value = "/scripts/out/ResourceQuestionControllerTest/addNewQuestionTrueSolvedTest/AfterTest.sql")

    public void addNewQuestionTrueSolvedTest() throws Exception {

        Long accountId = 102L;

        QuestionCreateRequestDto questionCreateRequestDto = new QuestionCreateRequestDto("My new question title",
                "try add my new question and test api",
                List.of(new TagResponseDto(1325L, "NewTag", "New tag description")));

        log.info("Запуск теста с accountId={}", accountId);


        when(authServiceFeignClient.isAccountExist(accountId)).thenReturn(true);

        MvcResult result = mockMvc.perform(post("/api/user/question?accountId=" + accountId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(questionCreateRequestDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.title").value("My new question title"))
                .andExpect(jsonPath("$.description").value("try add my new question and test api"))
                .andExpect(jsonPath("$.listTagDto[0].name").value("NewTag"))
                .andReturn();

        log.info("Тест успешно завершен: {}", result.getResponse().getContentAsString());
    }
    @Test
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:scripts/out/ResourceQuestionControllerTest.getAllQuestionCommentDtoById/BeforeTest.sql")
    @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:scripts/out/ResourceQuestionControllerTest.getAllQuestionCommentDtoById/AfterTest.sql")
    public void getQuestionComments_ShouldReturnValidComments() throws Exception {
        // Проверка данных в БД
        try (Connection conn = dataSource.getConnection();
             Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(
                    "SELECT COUNT(*) FROM comment_question WHERE question_id = 1"
            );
            rs.next();
            log.info("Comments in DB: {}", rs.getInt(1));
        }

        // Выполнение запроса и проверка результата
        mockMvc.perform(get("/api/user/question/1/comment"))
                .andDo(result -> log.debug("Response: {}", result.getResponse().getContentAsString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text", is("First test comment")))
                .andExpect(jsonPath("$[0].userId", is(100)))
                .andExpect(jsonPath("$[1].text", is("Second test comment")));
    }

    @Test
    public void getQuestionByIdInvalidQuestionIdTypeTest() throws Exception {
        mockMvc.perform(get("/api/user/question/invalid")
                        .param("accountId", "100"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getQuestionByIdInvalidAccountIdTypeTest() throws Exception {
        mockMvc.perform(get("/api/user/question/1")
                        .param("accountId", "invalid"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getQuestionByIdUserNotFoundTest() throws Exception {
        when(authServiceFeignClient.isAccountExist(100L)).thenReturn(false);

        mockMvc.perform(get("/api/user/question/100")
                        .param("accountId", "100"))
                .andExpect(status().isNotFound());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                    value = "classpath:scripts/out/ResourceQuestionControllerTest/getQuestionByIdSuccessTest/BeforeTest.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                    value = "classpath:scripts/out/ResourceQuestionControllerTest/getQuestionByIdSuccessTest/AfterTest.sql")
    })
    public void getQuestionByIdSuccessTest() throws Exception {
        when(authServiceFeignClient.isAccountExist(100L)).thenReturn(true);

        mockMvc.perform(get("/api/user/question/100")
                        .param("accountId", "100"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(100))
                .andExpect(jsonPath("$.title").value("Test Question"))
                .andExpect(jsonPath("$.authorId").value(100))
                .andExpect(jsonPath("$.authorName").value("testUser"))
                .andExpect(jsonPath("$.authorImage").value("https://example.com/image.jpg"))
                .andExpect(jsonPath("$.description").value("This is a test question"))
                .andExpect(jsonPath("$.viewCount").value(1))
                .andExpect(jsonPath("$.authorReputation").value(10))
                .andExpect(jsonPath("$.countAnswer").value(1))
                .andExpect(jsonPath("$.countValuable").value(1))
                .andExpect(jsonPath("$.persistDateTime").value("2023-10-01T12:00:00"))
                .andExpect(jsonPath("$.lastUpdateDateTime").value("2023-10-01T12:00:00"))
                .andExpect(jsonPath("$.countVote").value(1))
                .andExpect(jsonPath("$.voteTypeQuestion").value("UP"))
                .andExpect(jsonPath("$.listTagDto[0].id").value(100))
                .andExpect(jsonPath("$.listTagDto[0].name").value("testTag"))
                .andExpect(jsonPath("$.listTagDto[0].description").value("Test tag description"));
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                    value = "classpath:scripts/out/ResourceQuestionControllerTest/getQuestionByIdQuestionNotFoundTest/BeforeTest.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                    value = "classpath:scripts/out/ResourceQuestionControllerTest/getQuestionByIdQuestionNotFoundTest/AfterTest.sql")
    })
    public void getQuestionByIdQuestionNotFoundTest() throws Exception {
        when(authServiceFeignClient.isAccountExist(100L)).thenReturn(true);

        mockMvc.perform(get("/api/user/question/999")
                        .param("accountId", "100"))
                .andExpect(status().isNotFound());
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
                    value = "classpath:scripts/out/ResourceQuestionControllerTest/getQuestionByIdNotUserQuestionTest/BeforeTest.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD,
                    value = "classpath:scripts/out/ResourceQuestionControllerTest/getQuestionByIdNotUserQuestionTest/AfterTest.sql")
    })
    public void getQuestionByIdNotUserQuestionTest() throws Exception {
        when(authServiceFeignClient.isAccountExist(100L)).thenReturn(true);

        mockMvc.perform(get("/api/user/question/100")
                        .param("accountId", "101"))
                .andExpect(status().isNotFound());
    }
}