-- Удаляем связи между вопросами и тегами
DELETE FROM question_has_tag WHERE question_id IN (100, 101, 102, 103, 104, 105, 106, 107);

-- Удаляем вопросы
DELETE FROM question WHERE id IN (100, 101, 102, 103, 104, 105, 106, 107);

-- Удаляем пользователей
DELETE FROM user_entity WHERE account_id IN (100, 101, 102, 103, 104);

-- Удаляем теги
DELETE FROM tag WHERE id BETWEEN 100 AND 116;


