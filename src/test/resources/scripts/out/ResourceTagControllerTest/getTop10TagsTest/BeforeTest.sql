-- Добавляем теги
INSERT INTO tag (id, name, description, persist_date)
VALUES
    (100, 'Java', 'Java related tag', NOW()),
    (101, 'Spring', 'Spring Framework tag', NOW()),
    (102, 'API', 'API Design tag', NOW()),
    (103, 'REST', 'REST Architecture tag', NOW()),
    (104, 'Hibernate', 'Hibernate ORM tag', NOW()),
    (105, 'JPA', 'JPA Specification tag', NOW()),
    (106, 'Docker', 'Docker tag', NOW()),
    (107, 'Kubernetes', 'Kubernetes tag', NOW()),
    (108, 'Microservices', 'Microservices tag', NOW()),
    (109, 'CI/CD', 'CI/CD Practices tag', NOW()),
    (110, 'Cloud', 'Cloud Computing tag', NOW()),
    (111, 'Docker', 'Containerization with Docker', NOW()),
    (112, 'Kubernetes', 'Container Orchestration tag', NOW()),
    (113, 'DevOps', 'DevOps Practices tag', NOW()),
    (114, 'AWS', 'Amazon Web Services tag', NOW()),
    (115, 'Azure', 'Microsoft Azure cloud platform', NOW()),
    (116, 'GCP', 'Google Cloud Platform', NOW());

-- Добавляем пользователей
INSERT INTO user_entity (id, account_id, full_name, city, link_site, link_github, link_vk, about, image_link, nickname, persist_date, last_redaction_date)
VALUES
    (100, 100, 'John Doe', 'New York', 'http://example.com', 'http://github.com/johndoe', 'http://vk.com/johndoe', 'Java enthusiast', 'http://example.com/avatar.jpg', 'johndoe', NOW(), NOW()),
    (101, 101, 'Jane Smith', 'San Francisco', 'http://example.com', 'http://github.com/janesmith', 'http://vk.com/janesmith', 'Spring lover', 'http://example.com/avatar2.jpg', 'janesmith', NOW(), NOW()),
    (102, 102, 'Mike Johnson', 'Chicago', 'http://example.com', 'http://github.com/mikejohnson', 'http://vk.com/mikejohnson', 'REST API developer', 'http://example.com/avatar3.jpg', 'mikejohnson', NOW(), NOW()),
    (103, 103, 'Alice Williams', 'Los Angeles', 'http://example.com', 'http://github.com/alicewilliams', 'http://vk.com/alicewilliams', 'Cloud enthusiast', 'http://example.com/avatar4.jpg', 'alicew', NOW(), NOW()),
    (104, 104, 'Bob Brown', 'Boston', 'http://example.com', 'http://github.com/bobbrown', 'http://vk.com/bobbrown', 'DevOps professional', 'http://example.com/avatar5.jpg', 'bobbrown', NOW(), NOW());
-- Добавляем вопросы
INSERT INTO question (id, title, description, persist_date, account_id, last_redaction_date, is_deleted)
VALUES
    (100, 'Java Basics', 'Intro to Java', NOW(), 100, NOW(), FALSE),
    (101, 'Spring Guide', 'Using Spring Framework', NOW(), 101, NOW(), FALSE),
    (102, 'REST API', 'RESTful services', NOW(), 102, NOW(), FALSE),
    (103, 'Docker Basics', 'How to use Docker for containerization', NOW(), 103, NOW(), FALSE),
    (104, 'Kubernetes Introduction', 'Introduction to Kubernetes orchestration', NOW(), 104, NOW(), FALSE),
    (105, 'Cloud Computing', 'What is cloud computing?', NOW(), 103, NOW(), FALSE),
    (106, 'AWS Overview', 'Exploring AWS services', NOW(), 104, NOW(), FALSE),
    (107, 'Azure Fundamentals', 'Getting started with Microsoft Azure', NOW(), 104, NOW(), FALSE);
-- Связываем вопросы с тегами
INSERT INTO question_has_tag (question_id, tag_id)
VALUES
    (100, 100),
    (102, 100),
    (101, 100),
    (101, 101),
    (102, 102),
    (103, 111),
    (104, 112),
    (105, 110),
    (106, 114),
    (107, 115)
