INSERT INTO tag (id, name, description, persist_date)
VALUES (101, 'Tag 1', 'Description 1', '2024-08-04 00:00:00'),
       (102, 'Tag 2', 'Description 2', '2024-08-04 00:00:00'),
       (103, 'Tag 3', 'Description 3', '2024-08-04 00:00:00');

INSERT INTO user_entity (id, dtype, account_Id, full_name, persist_date, city, link_site, link_github, link_vk,
                         about, image_link, last_redaction_date, nickname)
VALUES (101, 'association', 1001, 'Full name 1', '2024-08-04 00:00:00', 'City 1', 'Link site 1', 'Link GitHub 1',
        'Link VK 1', 'About 1', 'Image link 1', '2020-01-01 00:00:00', 'Nickname 1'),
       (102, 'association', 1002, 'Full name 2', '2024-08-04 00:00:00', 'City 2', 'Link site 2', 'Link GitHub 2',
        'Link VK 2', 'About 2', 'Image link 2', '2020-01-01 00:00:00', 'Nickname 2');

INSERT INTO tag_ignore (id, ignored_tag_id, user_id, persist_date)
VALUES (101, 103, 102, '2024-08-04 00:00:00');

