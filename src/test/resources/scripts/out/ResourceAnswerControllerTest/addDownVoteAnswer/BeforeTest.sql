INSERT INTO user_entity (id, account_Id, full_name, persist_date, city, link_site, link_github, link_vk,
                         about, image_link, last_redaction_date, nickname, dtype)
VALUES (101, 101, 'John Doe', '2020-01-01T00:00:00', 'Moscow', 'https://www.stackover.com', 'https://github.com',
        'https://vk.com', 'About me', 'https://www.stackover.com/images.jpg', '2020-01-01T00:00:00', 'johndoe211345',
        'association'),
       (102, 102, 'Jane Doe', '2020-01-01T00:00:00', 'St. Petersburg', 'https://www.stackover.com',
        'https://github.com',
        'https://vk.com', 'About me', 'https://www.stackover.com/images.jpg', '2020-01-01T00:00:00', 'janedoe211',
        'association'),
       (103, 103, 'Jason Born', '2020-01-01T00:00:00', 'Los-Voronezh', 'https://www.stackover.com',
        'https://github.com',
        'https://vk.com', 'About me', 'https://www.stackover.com/images.jpg', '2020-01-01T00:00:00', 'jason_B',
        'association');

INSERT INTO reputation (id, persist_date, count, author_id, sender_id, type, question_id, answer_id)
VALUES (1001, '2020-01-01T00:00:00', 1, 101, 101, 1, null, null),
       (1002, '2020-01-01T00:00:00', 1, 101, 102, 1, null, null),
       (1003, '2020-01-01T00:00:00', 1, 101, 103, 1, null, null);

INSERT INTO question (id, account_id, title, description, persist_date, last_redaction_date, is_deleted)
VALUES (1010, 101, 'Question One', 'Description One', '2020-01-01T00:00:00', '2020-01-01T00:00:00', false);

INSERT INTO answer (id, account_id, question_id, persist_date, update_date, is_helpful, html_body, is_deleted,
                    is_deleted_by_moderator)
VALUES (1020, 101, 1010, '2020-01-01T00:00:00', '2020-01-01T00:00:00', true, 'HTML Body', false, false);

INSERT INTO votes_on_answers (id, user_id, answer_id, persist_date, vote_type_answer)
VALUES (1002, 102, 1020, '2020-01-01T00:00:00', 'DOWN');

