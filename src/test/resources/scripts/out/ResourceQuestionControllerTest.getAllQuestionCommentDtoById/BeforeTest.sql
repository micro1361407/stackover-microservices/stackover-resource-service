-- User
INSERT INTO user_entity (id, dtype, account_id, full_name, persist_date, city, link_site, link_github, link_vk, about, image_link, last_redaction_date, nickname)
VALUES (100, 'association', 100, 'Test User', CURRENT_TIMESTAMP, 'Test City',
        'http://test.com', 'http://github.com/test', 'http://vk.com/test',
        'Test user description', '/test.jpg', CURRENT_TIMESTAMP, 'test_user');

-- Tags
INSERT INTO tag (id, name, description, persist_date)
VALUES (1, 'Java', 'Java language tag', CURRENT_TIMESTAMP),
       (2, 'Spring', 'Spring Framework tag', CURRENT_TIMESTAMP);

-- Question
INSERT INTO question (id, account_id, title, description, persist_date,
                      last_redaction_date, is_deleted)
VALUES (1, 100, 'Test Question', 'Test Description', CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP, false);

-- Question-Tag relation
INSERT INTO question_has_tag (question_id, tag_id)
VALUES (1, 1), (1, 2);

-- Comments
INSERT INTO comment (id, text, comment_type, persist_date, last_redaction_date, user_id)
VALUES (1, 'First test comment', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 100),
       (2, 'Second test comment', 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 100);

-- Comment-Question relation
INSERT INTO comment_question (comment_text_id, question_id)
VALUES (1, 1), (2, 1);