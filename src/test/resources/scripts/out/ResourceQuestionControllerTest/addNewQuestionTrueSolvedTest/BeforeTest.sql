INSERT INTO tag (id, name, description, persist_date)
values
    ('100', 'FirstTag', 'AboutFirstTag', NOW()),
    ('101', 'KATA', 'AboutKata', NOW()),
    ('102', 'secondTag', 'AboutSecondTag', NOW());

INSERT INTO user_entity (id, account_Id, full_name, persist_date, city, link_site, link_github, link_vk,
                         about, image_link, last_redaction_date, nickname, dtype)
VALUES (101, 101, 'John Doe', '2020-01-01T00:00:00', 'Moscow', 'https://www.stackover.com', 'https://github.com',
        'https://vk.com', 'About me', 'https://www.stackover.com/images.jpg', '2020-01-01T00:00:00', 'johndoe211345',
        'association'),
       (102, 102, 'Jane Doe', '2020-01-01T00:00:00', 'St. Petersburg', 'https://www.stackover.com',
        'https://github.com',
        'https://vk.com', 'About me', 'https://www.stackover.com/images.jpg', '2020-01-01T00:00:00', 'janedoe211',
        'association'),
       (103, 103, 'Jason Born', '2020-01-01T00:00:00', 'Los-Voronezh', 'https://www.stackover.com',
        'https://github.com',
        'https://vk.com', 'About me', 'https://www.stackover.com/images.jpg', '2020-01-01T00:00:00', 'jason_B',
        'association');

INSERT INTO question (id, account_id, title, description, persist_date, last_redaction_date, is_deleted)
VALUES (1010, 101, 'Question One', 'Description One', '2020-01-01T00:00:00', '2020-01-01T00:00:00', false);

INSERT INTO question_has_tag (question_id, tag_id)
VALUES
    (1010, 100);
