-- Пользователь
INSERT INTO user_entity (id, account_id, full_name, persist_date, city, image_link, last_redaction_date, dtype)
VALUES (100, 100, 'testUser', '2023-10-01T12:00:00', 'NewYork', 'https://example.com/image.jpg', '2023-10-01T12:00:00',
        'association');

-- Вопрос
INSERT INTO question (id, title, description, persist_date, account_id, last_redaction_date)
VALUES (100, 'Test Question', 'This is a test question', '2023-10-01T12:00:00', 100, '2023-10-01T12:00:00');

-- Тег
INSERT INTO tag (id, name, description, persist_date)
VALUES (100, 'testTag', 'Test tag description', '2023-10-01T12:00:00');

-- Связь вопроса с тегом
INSERT INTO question_has_tag (question_id, tag_id)
VALUES (100, 100);

-- Репутация
INSERT INTO reputation (id, persist_date, author_id, sender_id, count, type)
VALUES (100, '2023-10-01T12:00:00', 100, 100, 10, 0);

-- Голос за вопрос
INSERT INTO votes_on_questions (id, user_id, question_id, persist_date, vote_type_question)
VALUES (100, 100, 100, '2023-10-01T12:00:00', 'UP');

-- Просмотр вопроса
INSERT INTO question_viewed (id, account_id, question_id, persist_date)
VALUES (100, 100, 100, '2023-10-01T12:00:00');

-- Ответ на вопрос
INSERT INTO answer (id, html_body, persist_date, update_date, is_helpful, is_deleted, is_deleted_by_moderator,
                    question_id, account_id)
VALUES (100, 'Test answer', '2023-10-01T12:00:00', '2023-10-01T12:00:00', true, false, false, 100, 100);