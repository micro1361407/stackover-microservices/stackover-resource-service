DELETE FROM answer WHERE id = 100;
DELETE FROM question_viewed WHERE id = 100;
DELETE FROM votes_on_questions WHERE id = 100;
DELETE FROM reputation WHERE id = 100;
DELETE FROM question_has_tag WHERE question_id = 100;
DELETE FROM tag WHERE id = 100;
DELETE FROM question WHERE id = 100;
DELETE FROM user_entity WHERE id = 100;