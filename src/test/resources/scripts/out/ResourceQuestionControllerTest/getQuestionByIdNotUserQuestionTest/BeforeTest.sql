-- Пользователь 1 (владелец вопроса)
INSERT INTO user_entity (id, account_id, full_name, persist_date, city, image_link, last_redaction_date, dtype)
VALUES (100, 100, 'testUser1', '2023-10-01T12:00:00', 'NewYork', 'https://example.com/image1.jpg', '2023-10-01T12:00:00', 'association');

-- Пользователь 2 (не владелец)
INSERT INTO user_entity (id, account_id, full_name, persist_date, city, image_link, last_redaction_date, dtype)
VALUES (101, 101, 'testUser2', '2023-10-01T12:00:00', 'Boston', 'https://example.com/image2.jpg', '2023-10-01T12:00:00', 'association');

-- Вопрос, принадлежащий пользователю 1
INSERT INTO question (id, title, description, persist_date, account_id, last_redaction_date)
VALUES (100, 'Test Question', 'This is a test question', '2023-10-01T12:00:00', 100, '2023-10-01T12:00:00');